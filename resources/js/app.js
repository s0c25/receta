import './bootstrap';
import Swal from 'sweetalert2';
import axios from 'axios';

window.Swal = Swal;
window.axios = axios;
