<x-app-layout>
<style>

</style>


       
  <h1 class="flex flex-row justify-center text-3xl capitalise mt-12">
            <p class="flex flex-row  text-3xl uppercase" >Glosario</p>
        </h1>

  <div class="container mx-auto text-justify text-sm sm:text-md ">

             <div class="grid grid-cols-2   justify-between ">
                 <div>
                 <div class="bg-red-400 p-2 rounded-md m-2" style="background:#FB718B;">Aderezo</div>
            <div class="bg-yellow-400 p-2 rounded-md m-2" style="background:#e6e6e6;">Salsas o condimentos que se usan para realzar el sabor de los platillos.</div>
            <div class=""></div>
                 </div>
                 <div>
                 <div class="bg-red-400 p-2 rounded-md m-2 " style="background:#FB718B;">Almuerzo</div>
            <div class="bg-yellow-400 p-2 rounded-md m-2" style="background:#e6e6e6;">Se refiere a la comida que se consume al mediodía, generalmente entre las 12:00 p. m. y las 3:00 p. m. Puede variar en contenido y estilo dependiendo de la región.</div>
            <div class=""></div>
                 </div>
                 <div>
                 <div class="bg-red-400 p-2 rounded-md m-2" style="background:#FB718B;">Aperitivo</div>
            <div class=" p-2 rounded-md m-2" style="background:#e6e6e6;">Son pequeñas porciones de comida que se sirven antes de la comida principal para estimular el apetito</div>
            <div class=""></div>
                 </div>
                 <div>
                 <div class="bg-red-400 p-2 rounded-md m-2" style="background:#FB718B;">Bebidas</div>
            <div class="bg-yellow-400 p-2 rounded-md m-2" style="background:#e6e6e6;">Abarcan una amplia gama de opciones,
desde bebidas alcohólicas, hasta
bebidas no alcohólicas como batidos,
limonada, té, entre otros.</div>
            <div class=""></div>
                 </div>
                 <div>
                 <div class="bg-red-400 p-2 rounded-md m-2 " style="background:#FB718B;">Cena</div>
            <div class="bg-yellow-400 p-2 rounded-md m-2" style="background:#e6e6e6;">Se refiere a la última comida del día,
entre el atardecer y la noche. Su
horario específico y cantidad de
alimento varía según la región y
cultura.</div>
<div class=""></div>
                 </div>

                <div>
                <div class="bg-red-400 p-2 rounded-md m-2" style="background:#FB718B;">Desayuno</div>
            <div class="bg-yellow-400 p-2 rounded-md m-2" style="background:#e6e6e6;">Es la primera comida del día,
generalmente ligera, que se toma por
la mañana. Puede variar según las
costumbres y preferencias culturales.</div>
            <div class=""></div>
                </div>

                <div>
                <div class="bg-red-400 p-2 rounded-md m-2" style="background:#FB718B;">Finger Food</div>
            <div class="bg-yellow-400 p-2 rounded-md m-2" style="background:#e6e6e6;">Se refiere a alimentos que se pueden
comer con los dedos, sin necesidad de
utilizar cubiertos. Se distinguen de los
aperitivos y otras comidas con
presentaciones similares por ser platos
completos pero sintetizados y
presentados en porciones pequeñas.</div>
            <div class=""></div>
                </div>

                <div>
                <div class="bg-red-400 p-2 rounded-md m-2" style="background:#FB718B;">Merienda</div>
            <div class="bg-yellow-400 p-2 rounded-md m-2" style="background:#e6e6e6;">La merienda es una comida ligera que
se toma a media tarde o media
mañana, antes del almuerzo o antes de
la cena.</div>
            <div class=""></div>
                </div>
                <div>
                <div class="bg-red-400 p-2 rounded-md m-2" style="background:#FB718B;">Postres</div>
            <div class="bg-yellow-400 p-2 rounded-md m-2"  style="background:#e6e6e6;">Se utiliza para designar a un tipo de
plato que se caracteriza por ser dulce
o agridulce y por servirse, por lo
general, al final de una cena o como
elemento principal en la merienda o
desayuno.</div>
                </div>
             </div>


             <!-- <h1 class="centro titulo ">
  Plan de comidas de dietas saludables
</h1>

<div class="w-full ">
    <table class="w-full border border-black">
        <thead>
            <tr>
                <th class="border"></th>
                <th class="border">Lunes</th>
                <th class="border">Martes</th>
                <th class="border">Miércoles</th>
                <th class="border">Jueves</th>
                <th class="border">Viernes</th>
                <th class="border">Sábado</th>
                <th class="border">Domingo</th>
            </tr>
        </thead>
        <tbody>

            <tr class="border">
                <td class="border "></td>
                <td class="border-black">
                  

                    <p>g</p>
   
                </td>
                <td class="border-black">
                 

                    <p></p>
   
                </td>
                <td class="border-black">
                   

                    <p></p>
   
                </td>
                <td class="border-black">
                 

                    <p></p>
   
                </td>
                <td class="border-black">
                   

                    <p></p>
   
                </td>
                <td class="border-black">
                 

                    <p></p>
   
                </td>
                <td class="border-black">
                 

                    <p>5</p>

                </td>
            </tr>

            <tr>
                <td colspan="8" class="border-black">
                    <p>5</p>
                    <p>5</p>
                    <p>5</p>
                </td>
            </tr>
        </tbody>
    </table>
</div> -->












            


            <!-- Agrega más términos y definiciones según sea necesario -->


  </x-app-layout>


