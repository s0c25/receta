<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Administrador Planificaciones ') }}
        </h2>
    </x-slot>

    <div wire:offline.class="bg-red-300" class=" lg:gap-8 p-6 lg:p-8">

        <section class="text-gray-400 body-font ">
            <div class="container">
                <div class="lg:w-2/3 w-full mx-auto ">
                    <div class="max-w-2xl overflow-hidden bg-white rounded-lg shadow-md dark:bg-gray-800">
                        <img class="object-cover w-full h-64" src="{{ asset('storage/' . $idReceta->foto_planificacion) }}" alt="Article">

                        <div class="p-6">
                            <div>
                            </div>
                            <div>
                                <div class="space-y-4">

                                    <details class="group rounded-lg bg-gray-50 p-6 [&_summary::-webkit-details-marker]:hidden" open>
                                        <summary class="flex cursor-pointer items-center justify-between gap-1.5 text-gray-900">
                                            <h2 class="font-medium">Recetas de la planificacion</h2>
                                            @php
                                            $totalCalorias = 0;

                                            foreach ($data as $detalle) {
                                                // Asegúrate de tener la propiedad 'calorias' en el objeto
                                                $calorias = $detalle->calorias ?? 0;

                                                // Sumar las calorías al total
                                                $totalCalorias += (int) $calorias;
                                            }
                                            @endphp

                                            <h2 class="font-medium">Calorias: {{ $totalCalorias }}</h2>
                                            
                                            <div>
                                                <label for="fechainicio">Fecha Inicio: {{$idReceta->fechainicio}}</label>
                                            </div>
                                            <div>
                                                <label for="fechafin">Fecha Fin: {{$idReceta->fechafin}} </label>
                                            </div>                                            



                                            <span class="relative h-5 w-5 shrink-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute inset-0 h-5 w-5 opacity-100 group-open:opacity-0" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>

                                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute inset-0 h-5 w-5 opacity-0 group-open:opacity-100" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                            </span>
                                        </summary>

                                        <div class="overflow-x-auto">
                                        <table class="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
                                                <thead class="ltr:text-left rtl:text-right">
                                                    <tr>
                                                        <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900 rounded-ss-md">Día</th>
                                                        <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">Nombre Receta</th>
                                                        <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900 rounded-se-md">Hora / Calorias Comida</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="divide-y divide-gray-200">
                                                    @if ($data && count($data) > 0)
                                                    @foreach($data as $row)
                                                    @php
                                                        $fecha = \Carbon\Carbon::parse($row->hora_comida);
                                                        $fecha->setLocale('es'); 
                                                        $nombreDia = $row->nombre;
                                                        $hora = $fecha->format('H:i');
                                                    @endphp

                                                    <tr>
                                                        <td class="whitespace-nowrap px-4 py-2 text-gray-700">{{$nombreDia}}</td>
                                                        <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">{{$row->nombre_receta}}</td>
                                                        <td class="whitespace-nowrap px-4 py-2 text-gray-700">{{$hora}} - {{$row->calorias  ?? 0}}</td>

                                                        
                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </details>
                                </div>
                            </div>

                            <div class="mt-4">
                                <div class="flex items-center">
                                    <div class="flex items-center">
                                        <img class="object-cover h-10 rounded-full" src="{{ asset('storage/' . Auth::user()->profile_photo_path) }}" alt="Avatar">
                                        <a class="mx-4 font-semibold text-gray-700 dark:text-gray-200" tabindex="0" role="link">Chef. {{ Auth::user()->name }} </a>
                                    </div>
                                    </br>
                                    <span class="mx-4 text-xs text-gray-600 dark:text-gray-300">{{ \Carbon\Carbon::parse(Auth::user()->created_at)->format('d F Y') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="flex">

                            <a href="{{ route('anterior.ante-net', ['id' => $idReceta->id]) }}" class="flex items-center px-4 py-2 mx-1 text-gray-500 bg-white rounded-md cursor-not-allowed dark:bg-gray-800 dark:text-gray-600">
                                Anterior
                            </a>
                            <a href="{{ route('siguiente.plani-net', ['id' => $idReceta->id]) }}" class="flex items-center px-4 py-2 mx-1 text-gray-700 transition-colors duration-300 transform bg-white rounded-md dark:bg-gray-800 dark:text-gray-200 hover:bg-blue-600 dark:hover:bg-blue-500 hover:text-white dark:hover:text-gray-200" onclick="cambiarPagina(1)">
                                Siguiente
                            </a>
                        </div>
                    </div>
                </div>
            </div>


        </section>

    </div>
    <script>
    let touchStartX = 0;
    let touchEndX = 0;

    document.addEventListener('touchstart', (e) => {
        touchStartX = e.touches[0].clientX;
    });

    document.addEventListener('touchmove', (e) => {
        touchEndX = e.touches[0].clientX;
    });

    document.addEventListener('touchend', (e) => {
        const windowHeight = window.innerHeight;
        const touchY = e.changedTouches[0].clientY;

        // Verifica si el deslizamiento ocurrió en la parte inferior de la pantalla
        if (touchY >= windowHeight * 0.8) { // Ajusta el valor según sea necesario
            handleSwipe();
        }
    });

    function handleSwipe() {
        const swipeThreshold = 50; // Umbral para considerar un gesto como un deslizamiento
        const deltaX = touchEndX - touchStartX;

        if (deltaX > swipeThreshold) {
            // Deslizamiento hacia la derecha, ejecutar acción de anterior
            window.location.href = "{{ route('anterior.ante-net', ['id' => $idReceta->id]) }}";
        } else if (deltaX < -swipeThreshold) {
            // Deslizamiento hacia la izquierda, ejecutar acción de siguiente
            window.location.href = "{{ route('siguiente.plani-net', ['id' => $idReceta->id]) }}";
        }
    }
</script>


    @include('sweetalert::alert')
</x-app-layout>