<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Administrar Planificación ') }}
        </h2>
    </x-slot>

    <div wire:offline.class="bg-red-300" class=" lg:gap-8 p-6 lg:p-8">

        <section class="text-gray-400 body-font ">
            <div class="container">
                <div class="lg:w-2/3 w-full mx-auto ">
                    <livewire:admin.show-planificacion  />
                </div>
            </div>
        </section>
    </div>
    @include('sweetalert::alert')
</x-app-layout>