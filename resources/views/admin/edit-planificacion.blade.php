<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            @role('admin')
            {{ __('Administrador Planificación ') }}
            @endrole
            @role('cliente')
            {{ __('Cliente Personalizar Planificación ') }}
            @endrole
        </h2>
    </x-slot>

    <div wire:offline.class="bg-red-300" class=" lg:gap-8 p-6 lg:p-8">

        <section class="text-gray-400 body-font ">

            <div class="container">
                <div class="lg:w-2/3 w-full mx-auto ">
                    <div class="max-w-2xl overflow-hidden bg-white rounded-lg shadow-md dark:bg-gray-800">
                        <img class="object-cover w-full h-64" src="{{ asset('storage/' . $idPlanificacion->foto_planificacion) }}" alt="Article">


                        <div class="p-6">
                            <div>
                                @role('admin')
                                <form enctype="multipart/form-data" id="formrecerta" method="POST" action="{{ route('admin.save-planificacion') }}">
                                    @csrf
                                    
                                    <div class="devide-y-2">
                                        <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white" for="foto_planificacion">Foto planificacion</label>
                                        <input name="foto_planificacion" class="block w-full text-sm text-gray-900 border
                                                        border-gray-300 rounded-lg cursor-pointer bg-gray-50 
                                                        dark:text-gray-400 focus:outline-none dark:bg-gray-700 
                                                        dark:border-gray-600 dark:placeholder-gray-400" id="foto_planificacion" type="file">
                            <label for="fechainicio">Fecha Inicio {{$idPlanificacion->fechainicio}} - Fecha Fin {{$idPlanificacion->fechafin}}</label>
                                    </div>

                                    @php
                                            $totalCalorias = 0;

                                            foreach ($data as $detalle) {
                                                // Asegúrate de tener la propiedad 'calorias' en el objeto
                                                $calorias = $detalle->calorias ?? 0;

                                                // Sumar las calorías al total
                                                $totalCalorias += (int) $calorias;
                                            }
                                            @endphp

                                            <h2 class="font-medium">Calorias: {{ $totalCalorias }}</h2>
                                    
                                    <label for="fechainicio">Fecha Inicio</label>
                                    <input id="fechainicio" class="w-auto" required type="date" name="fechainicio"  onchange="validarFormulario()">
                                    <label for="fechafin">Fecha Fin</label>
                                    <input id="fechafin" class="w-auto" required type="date" name="fechafin"  onchange="validarFormulario()">
                                </form>
                                @endrole
                            </div>
                            <div>
                                <div class="space-y-4">
                                    <div class="">
                                        <div class="container">
                                            <div class="grid grid-cols-2 gap-2 ">
                                                <div><span class="text-black text-xs sm:text-sm">Nombre Planificacion: <input class="bg-gray-100 w-11/12" readonly="readonly" disabled value="{{$idPlanificacion->nombre}}" type="text"></span><input type="hidden" value="{{$idPlanificacion->slug}}" name="slug"></div>
                                            </div>
                                            </form>
                                            <div class="flex flex-wrap ">
                                                <div style="overflow-y: auto;">

                                                    <table id="tablaplanificacionactivos" class=" min-w-full divide-y-2 divide-gray-200 bg-white text-sm mt-10">
                                                        <thead class="ltr:text-left rtl:text-right">
                                                            <tr>
                                                                <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900 rounded-ss-md">Nombre/Calorias receta </th>
                                                                <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">Categoria</th>
                                                                @role('admin')
                                                                <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">Hora Receta</th>
                                                                <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900 rounded-se-md">Opcion</th>
                                                                @endrole
                                                                @role('cliente')
                                                                <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">Fecha Receta</th>
                                                                @endrole

                                                            </tr>
                                                        </thead>
                                                        @role('admin')
                                                        <tbody class="divide-y divide-gray-200">
                                                            @if ($data && count($data) > 0)
                                                            @foreach($data as $row)
                                                            @php
                                                            $fecha = \Carbon\Carbon::parse($row->hora_comida);
                                                            $fecha->setLocale('es');
                                                            $nombreDia = $row->nombre; // Obtén el nombre completo del día de la semana en español
                                                            $hora = $fecha->format('H:i');
                                                            @endphp

                                                            <tr>
                                                            <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">{{$row->nombre_receta}} - {{$row->calorias  ?? 0}}</td>
                                                            <td class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">{{$row->nombre_categoria}}</td>
                                                            <td class="whitespace-nowrap px-4 py-2 text-gray-700">{{$nombreDia}} - {{$hora}}</td>
                                                            <td class="whitespace-nowrap px-4 py-2 text-gray-700"><button class="text-red-600 hover:underline" onclick="eliminarDato('{{ $row->id }}')">X</button></td>
                                                            

                                                            </tr>
                                                            @endforeach
                                                            @endif
                                                        </tbody>
                                                        @endrole

<!-- 
                                                        @role('cliente')
                                                        <tbody class="divide-y divide-gray-200">
                                                            @if ($data && count($data) > 0)
                                                            @foreach($data as $row)
                                                            @php
                                                            $fecha = \Carbon\Carbon::parse($row->hora_comida);
                                                            $fecha->setLocale('es');
                                                            $nombreDia = $row->nombre; // Obtén el nombre completo del día de la semana en español
                                                            $hora = $fecha->format('H:i');
                                                            @endphp

                                                            <tr>
                                                            <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">{{$row->nombre_receta}}</td>
                                                            <td class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">{{$row->nombre_categoria}}</td>
                                                            <td class="whitespace-nowrap px-4 py-2 text-gray-700">{{$nombreDia}} - {{$hora}}</td>

                                                            </tr>
                                                            @endforeach
                                                            @endif
                                                        </tbody>
                                                        @endrole -->

                                                    </table>
                                                </div>
                                            </div>

                                            <div class="">
                                            <!-- @role('cliente')
                                                <form id="formularioAgregar-cliente" >
                                                <label for="">Cambiar Fecha receta </label>

                                                    <div class="flex flex-row gap-2 mt-2">
                                                        <div class="divide-y-2">
                                                            <select name="recetas[]" class="recetasSelect w-11/12" onchange="validarFormularioCliente()">
                                                                @foreach ($data as $row)

                                                                <option value="{{ $row->id }}">{{ $row->nombre_receta}}</option>

                                                                @endforeach
                                                            </select>
                                                        </div>


                                                        <select name="dias[]" class="Dias w-11/12" onchange="validarFormularioCliente()">
                                                                @foreach ($dia as $row)
                                                                <option value="{{ $row->id }}">{{ $row->nombre}}</option>
                                                                @endforeach
                                                            </select>                                                        
                                                            <input class="w-11/12" required type="time" name="hora_inicio[]"  onchange="validarFormularioCliente()">


                                                    </div>
                                                </form>

                                                <div class="flex flex-wrap mt-2">
                                                    <button type="button" onclick="eliminarFila(this)">-</button> 
                                                    <div class="">
                                                        <button class="bg-black rounded-md p-2 m-2 text-white" id="guardarButton" onclick="guardarDatos_cliente()" disabled>Guardar</button>
                                                    </div>

                                                    <div class="">
                                                        <button class="bg-black rounded-md p-2 m-2 text-white" type="button" onclick="agregarFila_cliente()">Agregar</button>
                                                    </div>
                                                </div>
                                                @endrole -->


                                                @role('admin')
                                                <form id="formularioAgregar">
                                                    <div class="flex flex-row gap-2 mt-2">

                                                            <select name="recetas[]" class="recetasSelect w-11/12" onchange="validarFormulario()">
                                                                @foreach ($recetas as $receta)
                                                                <option value="{{ $receta->id }}">{{ $receta->nombre_receta}} - {{ $receta->nombre_categoria}}</option>
                                                                @endforeach
                                                            </select>


                                                        <select name="dias[]" class="Dias w-11/12" onchange="validarFormulario()">
                                                                @foreach ($dia as $row)
                                                                <option value="{{ $row->id }}">{{ $row->nombre}}</option>
                                                                @endforeach
                                                            </select>                                                        
                                                            <input class="w-11/12" required type="time" name="hora_inicio[]"  onchange="validarFormulario()">
                                              
                                                    </div>
                                                </form>
                                                <div class="flex flex-wrap mt-2">
                                                    <!-- <button type="button" onclick="eliminarFila(this)">-</button> -->
                                                    <div class="">
                                                       

                                                        <button class="bg-black rounded-md p-2 m-2 text-white" id="guardarButton" onclick="guardarDatos()" disabled>Guardar</button>
                                                    </div>

                                                    <div class="">
                                                        <button class="bg-black rounded-md p-2 m-2 text-white" type="button" onclick="agregarFila()">Agregar</button>
                                                    </div>
                                                </div>
                                                @endrole

                                                <div class="flex flex-wrap">
                                                    @role('admin')
                                                    <div class="btn mt-5"><a href="{{ route('admin.recetas')}}">
                                                            Regresar a recetas
                                                        </a>
                                                    </div>
                                                    @endrole

                                                    @role('cliente')
                                                    <div class="btn mt-5"><a href="{{ route('cliente.planificacion')}}">
                                                            Regresar a planificaciones
                                                        </a>
                                                    </div>
                                                    @endrole
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>


    @role('admin')

    <script>
        function validarFormulario() {
            var horaInicioInput = document.querySelector('input[name="hora_inicio[]"]');
            var recetasSelect = document.querySelector('select[name="recetas[]"]');
            var guardarButton = document.getElementById('guardarButton');
            var Dias = document.querySelector('select[name="dias[]"]');

            var isValid = horaInicioInput.value.trim() !== '' && Dias.value.trim() !== '' && recetasSelect.value.trim() !== '';
            guardarButton.disabled = !isValid;
        }
    </script>

    <script>
        function agregarFila() {
            const nuevaFila = document.createElement('div');
            nuevaFila.className = 'grid grid-cols-3';

            nuevaFila.innerHTML = `
            <div class="divide-y-2">
                <select name="recetas[]" class="recetasSelect w-11/12">
                    @foreach ($recetas as $receta)
                    <option value="{{ $receta->id }}">{{ $receta->nombre_receta}} - {{ $receta->nombre_categoria}}</option>
                    @endforeach
                </select>
            </div>

            <select name="dias[]" class="Dias w-11/12" onchange="validarFormulario()">
                                                                @foreach ($dia as $row)
                                                                <option value="{{ $row->id }}">{{ $row->nombre}}</option>
                                                                @endforeach
                                                            </select>  

            <div class="divide-y-2">
                    <input class="w-11/12" required type="time" name="hora_inicio[]">
            </div>
            <button type="button" onclick="eliminarFila(this)">Eliminar Fila</button>
        `;

            document.getElementById('formularioAgregar').appendChild(nuevaFila);
        }

        function eliminarFila(boton) {
            const fila = boton.parentNode;
            fila.parentNode.removeChild(fila);
        }

        function guardarDatos() {
            // Obtener la referencia al campo de entrada de archivo
            var inputFoto = document.getElementById('foto_planificacion');

            var inputfechainicio = document.getElementById('fechainicio');
            var inputfechafin = document.getElementById('fechafin');


            

            // Obtener el valor del campo de entrada de archivo
            var fotoSeleccionada = inputFoto.files[0];
            var fechainicio = inputfechainicio.value;
            var fechafin = inputfechafin.value;

            // Crear un objeto FormData
            const formData = new FormData(document.getElementById('formularioAgregar'));

            // Agrega el ID de receta a los datos antes de enviar
            formData.append('idPlanificacion', '{{ $idPlanificacion->id }}');

            // Verificar si hay una imagen seleccionada
            if (fotoSeleccionada) {
                // Si hay una imagen seleccionada, agregarla al FormData
                formData.append('foto_planificacion', fotoSeleccionada);
            } else {
                // Si no hay imagen seleccionada, agregar el campo con valor null
                formData.append('foto_planificacion', 'null');
            }

            formData.append('fechainicio', fechainicio);
            formData.append('fechafin', fechafin);

            axios.post('{{ route("save-planificacion") }}', formData)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                    actualizarSeccion();

                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        }

        function actualizarSeccion() {
            console.log('Se está llamando a actualizarSeccion');
            $('#tablaplanificacionactivos').load(location.href + ' #tablaplanificacionactivos', function() {
                console.log('Sección actualizada con éxito');
                // 

            });

        }
    </script>

    <script>
        function eliminarDato(id) {
            const url = '{{ route("eliminar.datos-planificacion", ["id" => ":id"]) }}';
            const apiUrl = url.replace(':id', id);
            const confirmacion = confirm('¿Estás seguro de que deseas eliminar este ingrediente?');
            axios.delete(apiUrl)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                    actualizarSeccion();
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        }

        function actualizarSeccion() {
            console.log('Se está llamando a actualizarSeccion');
            $('#tablaplanificacionactivos').load(location.href + ' #tablaplanificacionactivos', function() {
                console.log('Sección actualizada con éxito');


            });
        }
    </script>

    <script>
        document.getElementById('formrecerta').addEventListener('submit', function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            axios.post('{{ route("admin.save-planificacion") }}', formData)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        });
    </script>

    @endrole

    @include('sweetalert::alert')
</x-app-layout>