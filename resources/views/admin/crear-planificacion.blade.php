<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Administrador Recetas ') }}
        </h2>
    </x-slot>

    <div wire:offline.class="bg-red-300" class=" lg:gap-8 p-6 lg:p-8">

        <section class="text-gray-400 body-font ">
            
            <div class="container">
                <div class="lg:w-2/3 w-full mx-auto ">
                    <div class="max-w-2xl overflow-hidden bg-white rounded-lg shadow-md dark:bg-gray-800">
                            
                        <div class="p-6">
                            <div>
                            <div>
                                <div class="space-y-4">
                                    <div class="">
                                        

                                        <div class="flex flex-wrap">
                                        <table id="tablaingredientesactivos" class="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
                                            <thead class="ltr:text-left rtl:text-right">
                                                <tr>
                                                    <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900 rounded-ss-md">Ingredientes</th>
                                                    <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">Medidas</th>
                                                    <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">Cantidad</th>
                                                    <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900 rounded-se-md"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="divide-y divide-gray-200">
                                                @if ($data && count($data) > 0)
                                                @foreach($data as $row)
                                                <tr>
                                                    <td class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">{{$row->nombre_ingrediente}}</td>
                                                    <td class="whitespace-nowrap sm:px-4 sm:py-2 text-gray-700">{{$row->nombre_unidad}}</td>
                                                    <td class="whitespace-nowrap sm:px-4 sm:py-2 text-gray-700">{{$row->cantidad}}</td>
                                                    <td class="whitespace-nowrap sm:px-4 sm:py-2">
                                                        <button class="text-red-600 hover:underline" onclick="eliminarDato('{{ $row->idrecetaingrediente }}')">X</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        </div>

                                        <div class="">
                                        
                                        <!--///////////////////que si tomate y vainas de esa esto va debajo de la tabla   ////////////////////// -->
                                        <form id="formularioAgregar">
                                            <div class="flex flex-row gap-2 mt-2">
                                            
                                                <div class="divide-y-2">
                                                    <a href="#" class="text-black">
                                                        <select name="ingredientes[]" class="ingredienteSelect w-11/12">
                                                            @foreach ($ingredientes as $ingrediente)
                                                            <option value="{{ $ingrediente->id }}">{{ $ingrediente->nombre_ingrediente }}</option>
                                                            @endforeach
                                                        </select>
                                                    </a>
                                                </div>

                                                <div class="divide-y-2">
                                                    <a href="#" class="text-black">
                                                        <input class="w-11/12" type="text" name="cantidad[]">
                                                    </a>
                                                </div>

                                                
                                            </div>
                                            </form>


                                      
                                        <div class="flex flex-wrap mt-2">
                                            <button type="button" onclick="eliminarFila(this)">-</button>
                                            <div class="">
                                                <button class="bg-black rounded-md p-2 m-2 text-white" type="button" onclick="guardarDatos()">Guardar</button>
                                            </div>

                                            <div class="">
                                                <button class="bg-black rounded-md p-2 m-2 text-white" type="button" onclick="agregarFila()">Agregar</button>
                                            </div>
                                        </div>


                                        <div class="flex flex-wrap">
                                            <div class="btn mt-5"><a href="{{ route('admin.recetas')}}">
                                                    Regresar a recetas
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>


    <!-- Agregar ingrediente  y guardar -->
    <!-- Agrega este bloque de scripts al final del cuerpo de tu HTML antes de cerrar el body tag -->
    <script>
        function agregarFila() {
            const nuevaFila = document.createElement('div');
            nuevaFila.className = 'grid grid-cols-3';

            nuevaFila.innerHTML = `
            <div class="divide-y-2">
                <select name="ingredientes[]" class="ingredienteSelect">
                    @foreach ($ingredientes as $ingrediente)
                        <option value="{{ $ingrediente->id }}">{{ $ingrediente->nombre_ingrediente }}</option>
                    @endforeach
                </select>
            </div>
            <div class="divide-y-2">
                <select name="medidas[]" class="medidaSelect">
                    @foreach ($medidas as $medida)
                        <option value="{{ $medida->id }}">{{ $medida->nombre_unidad }}</option>
                    @endforeach
                </select>
            </div>
            <div class="divide-y-2">
                <input type="text" name="cantidad[]">
            </div>
            <button type="button" onclick="eliminarFila(this)">-</button>
        `;

            document.getElementById('formularioAgregar').appendChild(nuevaFila);
        }

        function eliminarFila(boton) {
            const fila = boton.parentNode;
            fila.parentNode.removeChild(fila);
        }

        function guardarDatos() {
            const formData = new FormData(document.getElementById('formularioAgregar'));

            // Agrega el ID de receta a los datos antes de enviar
            formData.append('idReceta', '{{ $idReceta->id }}');

            axios.post('{{ route("guardar.datos") }}', formData)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                    actualizarSeccion();
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        }

        function actualizarSeccion() {
            console.log('Se está llamando a actualizarSeccion');
            $('#tablaingredientesactivos').load(location.href + ' #tablaingredientesactivos', function() {
                console.log('Sección actualizada con éxito');
            });
        }
    </script>


    <!-- Eliminar ingrediente -->
    <script>
        function eliminarDato(id) {
            const url = '{{ route("eliminar.datos", ["id" => ":id"]) }}';
            const apiUrl = url.replace(':id', id);
            const confirmacion = confirm('¿Estás seguro de que deseas eliminar este ingrediente?');
            axios.delete(apiUrl)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                    actualizarSeccion();
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        }

        function actualizarSeccion() {
            console.log('Se está llamando a actualizarSeccion');
            $('#tablaingredientesactivos').load(location.href + ' #tablaingredientesactivos', function() {
                console.log('Sección actualizada con éxito');
            });
        }
    </script>

    <!-- Guardar Receta -->
    <script>
        document.getElementById('formrecerta').addEventListener('submit', function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            axios.post('{{ route("admin.save-receta") }}', formData)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        });
    </script>

    <!-- Touch para pwa -->

    @include('sweetalert::alert')
</x-app-layout>