<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Administrador Recetas ') }}
        </h2>
    </x-slot>

    <div wire:offline.class="bg-red-300" class=" lg:gap-8 p-6 lg:p-8">

        <section class="text-gray-400 body-font ">
            
            <div class="container">
                <div class="lg:w-2/3 w-full mx-auto ">
                    <div class="max-w-2xl overflow-hidden bg-white rounded-lg shadow-md dark:bg-gray-800">
                        <img class="object-cover w-full h-64" src="{{ asset('storage/' . $idReceta->foto_receta) }}" alt="Article">

                            
                        <div class="p-6">
                            <div>
                            <div class="devide-y-2">
                                                        <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white" for="foto_receta">Foto receta</label>
                                                        <input name="foto_receta" class="block w-full text-sm text-gray-900 border
                                                        border-gray-300 rounded-lg cursor-pointer bg-gray-50 
                                                        dark:text-gray-400 focus:outline-none dark:bg-gray-700 
                                                        dark:border-gray-600 dark:placeholder-gray-400" id="foto_receta" wire:model="foto_receta" type="file">
                                                    </div>
                            </div>
                            <div>
                                <div class="space-y-4">
                                    <div class="">
                                        <form enctype="multipart/form-data" id="formrecerta" method="POST" action="{{ route('admin.save-receta') }}">
                                            @csrf
                                            <div class="container">
                                                <div class="grid grid-cols-2 gap-2 ">
                                                    <div><span class="text-black text-xs sm:text-sm">Nombre Receta: <input class="bg-gray-100 w-11/12" readonly="readonly" disabled value="{{$idReceta->nombre_receta}}" type="text"></span><input type="hidden" value="{{$idReceta->slug}}" name="slug"></div>
                                                    <div><span class="text-black text-xs sm:text-sm">Cantidad de Personas </span> <input required class="w-11/12" type="text" name="porciones" value="{{$idReceta->porciones}}"></div>
                                                </div>
                                                <!--///////////////////segundo segmento  ////////////////////// -->
                                                <div class="grid grid-cols-3 gap-1 text-center mt-2 ">

                                            
                                                    <div class="devide-y-2 ">
                                                    <span class="text-black p-5"><span class="text-black text-xs sm:text-sm">Tiempo Preparación</span> <input required class="w-11/12  rounded-md" type="text" name="tiempo_preparacion" value="{{$idReceta->tiempo_preparacion}}">
                                                    </div>
                                                    <div class="devide-y-2  ">
                                                        <label for=" " class=" text-xs sm:text-sm">Seleciona Categoria</label>
                                                        <select class="w-11/12  rounded-md border border-black" required name="categoria" id="">
                                                            @foreach($categoria as $row)
                                                            <option value="{{$row->id}}">{{$row->nombre_categoria}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="devide-y-2 mt-6 sm:mt-0 ">
                                                    <span class="text-black text-xs sm:text-sm ">Calorias</span><input required type="text" class="w-11/12  rounded-md" name="calorias" value="{{$idReceta->calorias}}">
                                                    </div>

                                                    <div class="col-start-1 col-span-3 m-2">
                                                        <span class="text-xs md:text-sm text-center">Preparación:</span>
                                                        <textarea class="w-full rounded-md " required name="preparacion" id="preparacion" cols="70" rows="2">{{$idReceta->preparacion}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="flex flex-wrap">
                                                    <div class="btn bg-black text-white justify-start rounded-md p-3 m-1">
                                                        <button type="submit">Guardar</button>
                                                    </div>
                                                </div>
                                        </form>

                                        <div class="flex flex-wrap">
                                        <table id="tablaingredientesactivos" class="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
                                            <thead class="ltr:text-left rtl:text-right">
                                                <tr>
                                                <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900 rounded-ss-md">Ingredientes</th>
                                                    <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">Medidas</th>
                                                    <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">Cantidad</th>
                                                    <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900 rounded-se-md"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="divide-y divide-gray-200">
                                                @if ($data && count($data) > 0)
                                                @foreach($data as $row)
                                                <tr>
                                                    <td class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">{{$row->nombre_ingrediente}}</td>
                                                    <td class="whitespace-nowrap sm:px-4 sm:py-2 text-gray-700">{{$row->nombre_unidad}}</td>
                                                    <td class="whitespace-nowrap sm:px-4 sm:py-2 text-gray-700">{{$row->cantidad}}</td>
                                                    <td class="whitespace-nowrap sm:px-4 sm:py-2">
                                                        <button class="text-red-600 hover:underline" onclick="eliminarDato('{{ $row->idrecetaingrediente }}')">X</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        </div>

                                        <div class="">
                                        
                                        <!--///////////////////que si tomate y vainas de esa esto va debajo de la tabla   ////////////////////// -->
                                        <form id="formularioAgregar">
                                            <div class="flex flex-row gap-2 mt-2">
                                            
                                                <div class="divide-y-2">
                                                        <select name="ingredientes[]" class="ingredienteSelect w-11/12">
                                                            @foreach ($ingredientes as $ingrediente)
                                                            <option value="{{ $ingrediente->id }}">{{ $ingrediente->nombre_ingrediente }}</option>
                                                            @endforeach
                                                        </select>
                                                </div>

                                                <div class="divide-y-2">
                                                        <select name="medidas[]" class="medidaSelect w-11/12">
                                                            @foreach ($medidas as $medida)
                                                            <option value="{{ $medida->id }}">{{ $medida->nombre_unidad }}</option>
                                                            @endforeach
                                                        </select>
                                                </div>

                                                <div class="divide-y-2">
                                                        <input class="w-11/12" type="text" name="cantidad[]">
                                                </div>

                                                
                                            </div>
                                            </form>


                                      
                                        <div class="flex flex-wrap mt-2">
                                            <button type="button" onclick="eliminarFila(this)">-</button>
                                            <div class="">
                                                <button class="bg-black rounded-md p-2 m-2 text-white" type="button" onclick="guardarDatos()">Guardar</button>
                                            </div>

                                            <div class="">
                                                <button class="bg-black rounded-md p-2 m-2 text-white" type="button" onclick="agregarFila()">Agregar</button>
                                            </div>
                                        </div>

                                        <!--///////////////////el nombre de el chef  ////////////////////// -->

                                        <div class="mt-4">
                                            <div class="flex items-center">
                                                <div class="flex items-center">
                                                    <img class="object-cover h-10 rounded-full" src="{{ asset('storage/' . Auth::user()->profile_photo_path) }}" alt="Avatar">
                                                    <a href="#" class="mx-4 font-semibold text-gray-700 dark:text-gray-200" tabindex="0" role="link">Chef. {{ Auth::user()->name }} </a>
                                                </div>
                                                </br>
                                                <span class="mx-4 text-xs text-gray-600 dark:text-gray-300">{{
                                                        \Carbon\Carbon::parse(Auth::user()->created_at)->format('d F Y')
                                                        }}</span>
                                            </div>

                                        </div>
                                        <div class="flex flex-wrap">
                                            <div class="btn mt-5"><a href="{{ route('admin.recetas')}}">
                                                    Regresar a recetas
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>


    <!-- Agregar ingrediente  y guardar -->
    <!-- Agrega este bloque de scripts al final del cuerpo de tu HTML antes de cerrar el body tag -->
    <script>
        function agregarFila() {
            const nuevaFila = document.createElement('div');
            nuevaFila.className = 'grid grid-cols-3';

            nuevaFila.innerHTML = `
            <div class="divide-y-2">
                <select name="ingredientes[]" class="ingredienteSelect">
                    @foreach ($ingredientes as $ingrediente)
                        <option value="{{ $ingrediente->id }}">{{ $ingrediente->nombre_ingrediente }}</option>
                    @endforeach
                </select>
            </div>
            <div class="divide-y-2">
                <select name="medidas[]" class="medidaSelect">
                    @foreach ($medidas as $medida)
                        <option value="{{ $medida->id }}">{{ $medida->nombre_unidad }}</option>
                    @endforeach
                </select>
            </div>
            <div class="divide-y-2">
                <input type="text" name="cantidad[]">
            </div>
            <button type="button" onclick="eliminarFila(this)">-</button>
        `;

            document.getElementById('formularioAgregar').appendChild(nuevaFila);
        }

        function eliminarFila(boton) {
            const fila = boton.parentNode;
            fila.parentNode.removeChild(fila);
        }

        function guardarDatos() {
            const formData = new FormData(document.getElementById('formularioAgregar'));

            // Agrega el ID de receta a los datos antes de enviar
            formData.append('idReceta', '{{ $idReceta->id }}');

            axios.post('{{ route("guardar.datos") }}', formData)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                    actualizarSeccion();
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        }

        function actualizarSeccion() {
            console.log('Se está llamando a actualizarSeccion');
            $('#tablaingredientesactivos').load(location.href + ' #tablaingredientesactivos', function() {
                console.log('Sección actualizada con éxito');
            });
        }
    </script>


    <!-- Eliminar ingrediente -->
    <script>
        function eliminarDato(id) {
            const url = '{{ route("eliminar.datos", ["id" => ":id"]) }}';
            const apiUrl = url.replace(':id', id);
            const confirmacion = confirm('¿Estás seguro de que deseas eliminar este ingrediente?');
            axios.delete(apiUrl)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                    actualizarSeccion();
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        }

        function actualizarSeccion() {
            console.log('Se está llamando a actualizarSeccion');
            $('#tablaingredientesactivos').load(location.href + ' #tablaingredientesactivos', function() {
                console.log('Sección actualizada con éxito');
            });
        }
    </script>

    <!-- Guardar Receta -->
    <script>
        document.getElementById('formrecerta').addEventListener('submit', function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            axios.post('{{ route("admin.save-receta") }}', formData)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        });
    </script>

    <!-- Touch para pwa -->

    @include('sweetalert::alert')
</x-app-layout>