<div>
    <button wire:click="$set('open',true)" class="flex items-center justify-center p-2 mx-1 text-xs sm:text-sm text-sm tracking-wide text-white transition-colors duration-200 bg-blue-500 rounded-lg shrink-0 sm:w-auto gap-x-2 hover:bg-blue-600 dark:hover:bg-blue-500 dark:bg-blue-600">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Busqueda Avanzada</span>
    </button>

    <x-dialog-modal wire:model.live="open">

        <x-slot name="title">Selecciona tus datos</x-slot>
        <x-slot name="content">

            <form wire:submit.prevent="realizarBusqueda">
                <label for="selectIngrediente" class="block text-sm font-medium text-gray-700 mt-2">Selecciona Ingredientes</label>

                <select wire:model="selectIngrediente" multiple class="block w-full mt-1 form-multiselect rounded-md border border-black">
                    @foreach ($ingredientes as $ingrediente)
                    <option value="{{ $ingrediente->id }}">{{ $ingrediente->nombre_ingrediente }}</option>
                    @endforeach
                </select>

                <label for="selectcalorias" class="block text-sm font-medium text-gray-700 mt-2">Selecciona Calorias</label>
                <select wire:model="selectcalorias"  class="block w-full mt-1 form-multiselect rounded-md border border-black">
                    <option value="- 500">Menor 500</option>
                    <option value="+ 500">Mayor 500</option>
                </select>
                
                <label for="selectIngrediente" class="block text-sm font-medium text-gray-700 mt-2">Selecciona Tiempo Preparación</label>
                <select wire:model="selectcoccion"  class="block w-full mt-1 form-multiselect rounded-md border border-black">
                    <option value="Mayor 20">Mayor 20 min</option>
                    <option value="Menor 20">Menor 20 min</option>
                </select>

                <button type="submit" class="p-4 py-2 rounded-md bg-black text-blod text-white mt-3 mb-3">Consultar</button>

            </form>

            <x-validation-errors class="mb-4" />

            <table class="table-fixed">
                <thead>
                    <tr>
                        <th>Nombre Receta</th>
                        <th>Calorias</th>
                        <th>Cocción</th>
                        <th>Porciones</th>
                        <th>Opciones</th>

                    </tr>
                </thead>

                <tbody>
                    @if ($datas && count($datas) > 0)

                    @foreach ($datas as $datosReceta)

                    <tr>

                        <td>{{ $datosReceta['nombre_receta'] }}</td>
                        <td>{{ $datosReceta['calorias'] }}</td>
                        <td>{{ $datosReceta['tiempo_preparacion'] }}</td>
                        <td>
                        <a href="{{ route('cliente.recetas-imprimir', ['row' => $datosReceta['id']  ]) }}">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M6.72 13.829c-.24.03-.48.062-.72.096m.72-.096a42.415 42.415 0 0 1 10.56 0m-10.56 0L6.34 18m10.94-4.171c.24.03.48.062.72.096m-.72-.096L17.66 18m0 0 .229 2.523a1.125 1.125 0 0 1-1.12 1.227H7.231c-.662 0-1.18-.568-1.12-1.227L6.34 18m11.318 0h1.091A2.25 2.25 0 0 0 21 15.75V9.456c0-1.081-.768-2.015-1.837-2.175a48.055 48.055 0 0 0-1.913-.247M6.34 18H5.25A2.25 2.25 0 0 1 3 15.75V9.456c0-1.081.768-2.015 1.837-2.175a48.041 48.041 0 0 1 1.913-.247m10.5 0a48.536 48.536 0 0 0-10.5 0m10.5 0V3.375c0-.621-.504-1.125-1.125-1.125h-8.25c-.621 0-1.125.504-1.125 1.125v3.659M18 10.5h.008v.008H18V10.5Zm-3 0h.008v.008H15V10.5Z" />
                                </svg>

                            </a>
                        </td>

                        <td>
                            <a href="{{ route('cliente.ver-recetas-cliente', ['row' => $datosReceta['id'] ]) }}"  target="_blank">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                                </svg>



                            </a>
                        </td>
                      

                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>



        </x-slot>
        <x-slot name="footer">


            <button type="button" wire:click="cerrar" class="p-4 py-2  rounded-md bg-black text-blod text-white ml-3">Cerrar</button>

        </x-slot>


    </x-dialog-modal>


</div>