<div>

  <div class="w-64">
    <div class="row">
      <div class="col">

        <div>
          <table class="table table-bordered align-items-center table-sm">
            <thead class="thead-light">
              <tr>
                <th>Ingrediente</th>
                <th>Medida</th>
                <th>Cantidad</th>
              </tr>
            </thead>
            <tbody>
              @foreach($campos as $index => $campo)
              <tr>
                <td>
                  <input wire:model="campos.{{ $index }}.ingrediente" type="text" name="ingrediente[]">
                </td>
                <td>
                  <input wire:model="campos.{{ $index }}.medida" type="text" name="medida[]">
                </td>
                <td>
                  <input wire:model="campos.{{ $index }}.cantidad" type="text" name="cantidad[]">
                </td>
                <td>
                  <button wire:click="eliminarCampo({{ $index }})">-</button>
                </td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <td colspan="4" class="text-right">
                  <a wire:click="agregarCampo" class="btn btn-info"> +</a>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>

      </div>
    </div>
  </div>

</div>