<div>
    <button wire:click="$set('open',true)" style=" background:#FB718B;"class="flex items-center justify-center p-2 mx-1 text-sm text-sm tracking-wide text-white transition-colors duration-200 bg-blue-500 rounded-lg shrink-0 sm:w-auto gap-x-2 bg-blue-900">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Agregar <br> Receta</span>
    </button>

    <x-dialog-modal wire:model.live="open">

        <x-slot name="title">Rellenar todos los para la receta</x-slot>
        <x-slot name="content">

            <x-validation-errors class="mb-4" />

            <form method="POST" action="{{ route('admin.recetas-save') }}" enctype="multipart/form-data">
                @csrf
                <div class="mt-4">

                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white" for="foto_receta">Upload file</label>
                    <input required name="foto_receta" class="block w-full text-sm text-gray-900 border
 border-gray-300 rounded-lg cursor-pointer bg-gray-50 
 dark:text-gray-400 focus:outline-none dark:bg-gray-700 
 dark:border-gray-600 dark:placeholder-gray-400" id="foto_receta" wire:model="foto_receta" type="file">

                </div>

                <div>
                    <x-label for="nombre_receta" value="{{ __('Nombre Receta') }}" />
                    <x-input required wire:model="nombre_receta" id="nombre_receta" class="block mt-1 w-full" type="text" name="nombre_receta"  required autofocus autocomplete="nombre_receta" />
                </div>
                <div class="mt-4">
                    <x-label for="tiempo_preparacion" value="{{ __('Tiempo Preparacion') }}" />
                    <x-input required wire:model="tiempo_preparacion" id="tiempo_preparacion" class="block mt-1 w-full" type="text" name="tiempo_preparacion"  required autofocus autocomplete="tiempo_preparacion" />
                </div>

                <div class="mt-4">
                    <x-label for="porciones" value="{{ __('Porciones') }}" />
                    <x-input wire:model="porciones" id="porciones" class="block mt-1 w-full" type="text" name="porciones"  required autofocus autocomplete="porciones" />
                </div>

                <div class="mt-4">
                    <x-label for="calorias" value="{{ __('Calorías') }}" />
                    <x-input wire:model="calorias" id="calorías" class="block mt-1 w-full" type="text" name="calorias"  required autofocus autocomplete="calorías" />
                </div>

                <div class="mt-4">
                    <x-label for="categoria" value="{{ __('Categoria Receta') }}" />
                    <select required wire:model="categoria" id="categoria" name="categoria" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected disabled>Selecciona Categoria</option>
                        @if ($categoria && count($categoria) > 0)
                        @foreach($categoria as $row)
                        <option value="{{$row->id}}">{{$row->nombre_categoria}}</option>
                        @endforeach
                        @endif
                    </select>

                </div>

                <div class="mt-4">
                    <x-label for="preparacion" value="{{ __('Preparación Receta') }}" />
                    <textarea required name="preparacion"  wire:model="preparacion" id="preparacion" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Describe el paso a paso..."></textarea>
                </div>
                <div class="mt-4">
                    @livewire('admin.opcion-add-recetas')
                </div>




        </x-slot>
        <x-slot name="footer">
            <x-action-message class="me-3" on="saved">
                {{ __('Saved.') }}
            </x-action-message>

            <x-button>
                {{ __('Save') }}
            </x-button> 
            
            <button type="button" wire:click="cerrar" class="p-4 py-2  rounded-md bg-black text-blod text-white ml-3">Cerrar</button>
        
        </x-slot>
        </form>


    </x-dialog-modal>
</div>