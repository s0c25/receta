<div>
    <button wire:click="$set('open',true)" class="flex items-center justify-center p-2 mx-1 text-sm text-sm tracking-wide text-white transition-colors duration-200 bg-blue-500 rounded-lg shrink-0 sm:w-auto gap-x-2 hover:bg-blue-600 dark:hover:bg-blue-500 dark:bg-blue-600">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Agregar <br> Planificacion</span>
    </button>

    <x-dialog-modal wire:model.live="open">

        <x-slot name="title">Crear la Planificación</x-slot>
        <x-slot name="content">

            <x-validation-errors class="mb-4" />

            <form method="POST" action="{{ route('admin.planificacion-guarda') }}" enctype="multipart/form-data">
                @csrf
                <div class="mt-4">
                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-white" for="foto_receta">Upload file</label>
                    <input required name="foto_planificacion" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50  dark:text-gray-400 focus:outline-none dark:bg-gray-700  dark:border-gray-600 dark:placeholder-gray-400" id="foto_planificacion" type="file">
                </div>

                <div class="mt-4">
                    <x-label for="nombreplanificacion" value="{{ __('Nombre Planificacion') }}" />
                    <input class="w-11/12 md:w-auto" wire:model="nombre_planificacion" type="text" name="nombre_planificacion">
                </div>

        </x-slot>
        <x-slot name="footer">
            <x-action-message class="me-3" on="saved">
                {{ __('Saved.') }}
            </x-action-message>

            <x-button>
                {{ __('Guardar') }}
            </x-button>

            <button type="button" wire:click="cerrar" class="p-4 py-2  rounded-md bg-black text-blod text-white ml-3">Cerrar</button>

        </x-slot>
        </form>


    </x-dialog-modal>
</div>