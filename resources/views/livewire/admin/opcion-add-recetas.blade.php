<div>

  <div class="w-auto">
    <div class="flex flex-row">
      <div class="">

        <div class="">
          <table class="table table-bordered align-items-center table-sm">
            <thead class="thead-light">
              <tr>
                <th>Ingrediente</th>
                <th>Medida</th>
                <th>Cantidad</th>
              </tr>
            </thead>
            <tbody>
              @foreach($campos as $index => $campo)
              <tr>
                <td>

                  <select class="w-11/12 md:w-auto" wire:model="campos.{{ $index }}.ingrediente" name="ingrediente[]">
                    @if ($ingredientes && count($ingredientes) > 0)
                    @foreach($ingredientes as $row)
                    <option value="{{$row->id}}">{{$row->nombre_ingrediente}}</option>
                    @endforeach
                    @endif

                  </select>
                </td>
                <td>
                  <select  class="w-11/12 md:w-auto" wire:model="campos.{{ $index }}.medida" name="medida[]">
                    @if ($medidas && count($medidas) > 0)
                    @foreach($medidas as $row)
                    <option value="{{$row->id}}">{{$row->nombre_unidad}}</option>
                    @endforeach
                    @endif


                  </select>
                </td>
                <td>
                  <input class="w-11/12 md:w-auto" wire:model="campos.{{ $index }}.cantidad" type="text" name="cantidad[]">
                </td>
                <td>
                  <button wire:click="eliminarCampo({{ $index }})">-</button>
                </td>
              </tr>
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <td colspan="4" class="text-right">
                 
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
      <a wire:click="agregarCampo" class="btn btn-info ml-5 mb-5 bg-blue-200 rounded-full p-1"> +</a>
    </div>
  </div>



</div>