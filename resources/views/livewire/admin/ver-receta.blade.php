<div>
    <div wire:loading>
        Cargando...
    </div>
    <section class="container px-auto mx-auto">
    <div class="flex flex-wrap">
            <h2 class="text-lg font-medium text-gray-800 dark:text-white s">Recetas Activas</h2>
            <div>
                <div class="flex flex-row ">
                    <span class="px-3 py-1 text-xs text-blue-600 bg-blue-100 rounded-full dark:bg-gray-800 dark:text-blue-400">
                        @if(!empty($count))
                        {{ $count }}
                        @endif
                    </span>
                </div>
            </div>
            </div>
        <div class="flex flex-wrap justify-center ">


            <div class=" flex flex-row mt-4  ">
                @livewire('items-nuevos')

                @livewire('upload-recetas')

                @livewire('admin.add-receta')
            </div>
        </div>


        <div class="mt-6 flex flex-row items-center justify-center">
            <!-- <div class="inline-flex overflow-hidden bg-white border divide-x rounded-lg dark:bg-gray-900 rtl:flex-row-reverse dark:border-gray-700 dark:divide-gray-700">
            <button class="px-5 py-2 text-xs font-medium text-gray-600 transition-colors duration-200 bg-gray-100 sm:text-sm dark:bg-gray-800 dark:text-gray-300">
                View all
            </button>

            <button class="px-5 py-2 text-xs font-medium text-gray-600 transition-colors duration-200 sm:text-sm dark:hover:bg-gray-800 dark:text-gray-300 hover:bg-gray-100">
                Monitored
            </button>

            <button class="px-5 py-2 text-xs font-medium text-gray-600 transition-colors duration-200 sm:text-sm dark:hover:bg-gray-800 dark:text-gray-300 hover:bg-gray-100">
                Unmonitored
            </button>
        </div> -->
         <div class="">
             <div class="relative flex items-center mt-4 md:mt-0">
                <span class="absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5 mx-3 text-gray-400 dark:text-gray-600">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                    </svg>
                </span>

                <input type="text" placeholder="Buscar" wire:model="searchTerm" wire:keydown.debounce.500ms='search' class="block w-full py-1.5 pr-5 text-gray-700
              bg-white border border-gray-200  rounded-l-md
              md:w-80 placeholder-gray-400/70 pl-11 rtl:pr-11 
              rtl:pl-5 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-600 
              focus:border-blue-400 dark:focus:border-blue-300 focus:ring-blue-300 
              focus:outline-none focus:ring focus:ring-opacity-40 md:justify-center ">
            </div>
        </div>
        
         </div>
        

        <div class="flex flex-wrap mt-6  justify-center">
            <div class="-mx-4 -my-2 overflow-x sm:-mx-6 lg:-mx-8">
                <div class="min-w-full py-2 align-middle md:px-6 lg:px-8">
                    <div class="overflow-x border border-gray-200 dark:border-gray-700 md:rounded-lg">
                        <table class="table-fixed min-w-full divide-y divide-gray-200  rounded-lg">
                            <thead class="bg-gray-50 dark:bg-gray-800">
                                <tr>
                                    <th scope="col" class="m-1 p-1 md:p-5 md:m-3 rounded-s-md">
                                        Imagen
                                    </th>

                                    <th scope="col" class="py-3.5 px-4 text-sm   text-center  justify-center ">
                                        <button wire:click="toggleOrderType" class="flex items-center gap-x-1 ">
                                        Recetas Activas

                                            <svg class="h-3" viewBox="0 0 10 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M2.13347 0.0999756H2.98516L5.01902 4.79058H3.86226L3.45549 3.79907H1.63772L1.24366 4.79058H0.0996094L2.13347 0.0999756ZM2.54025 1.46012L1.96822 2.92196H3.11227L2.54025 1.46012Z" fill="currentColor" stroke="currentColor" stroke-width="0.1" />
                                                <path d="M0.722656 9.60832L3.09974 6.78633H0.811638V5.87109H4.35819V6.78633L2.01925 9.60832H4.43446V10.5617H0.722656V9.60832Z" fill="currentColor" stroke="currentColor" stroke-width="0.1" />
                                                <path d="M8.45558 7.25664V7.40664H8.60558H9.66065C9.72481 7.40664 9.74667 7.42274 9.75141 7.42691C9.75148 7.42808 9.75146 7.42993 9.75116 7.43262C9.75001 7.44265 9.74458 7.46304 9.72525 7.49314C9.72522 7.4932 9.72518 7.49326 9.72514 7.49332L7.86959 10.3529L7.86924 10.3534C7.83227 10.4109 7.79863 10.418 7.78568 10.418C7.77272 10.418 7.73908 10.4109 7.70211 10.3534L7.70177 10.3529L5.84621 7.49332C5.84617 7.49325 5.84612 7.49318 5.84608 7.49311C5.82677 7.46302 5.82135 7.44264 5.8202 7.43262C5.81989 7.42993 5.81987 7.42808 5.81994 7.42691C5.82469 7.42274 5.84655 7.40664 5.91071 7.40664H6.96578H7.11578V7.25664V0.633865C7.11578 0.42434 7.29014 0.249976 7.49967 0.249976H8.07169C8.28121 0.249976 8.45558 0.42434 8.45558 0.633865V7.25664Z" fill="currentColor" stroke="currentColor" stroke-width="0.3" />
                                            </svg>
                                        </button>
                                    </th>

                                    <th scope="col" class="m-1 p-1 md:p-5 md:m-3">
                                        Categoria
                                    </th>


                                    <th scope="col" class="m-1 p-1 md:p-5 md:m-3 rounded-se-md">
                                        Opciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
                                @if ($recetas && count($recetas) > 0)
                                @foreach($recetas as $row)
                                <tr>
                                    <td class="md:p-5 md:m-3">
                                        <div class="flex items-center">

                                            <img srcset="{{ asset('storage/' . $row->foto_receta) }}" class="w-full h-auto max-w-xl rounded-lg" alt="image description">

                                        </div>
                                    </td>

                                    <td class="md:p-5 md:m-3">
                                        <div>
                                            <h2 class="font-medium text-gray-800 dark:text-white text-center ">{{$row->nombre_receta}}</h2>
                                            <p class="text-sm font-normal text-gray-600 dark:text-gray-400 text-center ">{{$row->porciones}}</p>
                                        </div>
                                    </td>
                                    <td class="md:p-5 md:m-3">
                                        <div class=" mx-auto inline px-3 py-1 text-sm font-normal rounded-full text-emerald-500 gap-x-2 bg-emerald-100/60 dark:bg-gray-800">
                                            {{$row->categoria->nombre_categoria}}
                                        </div>
                                    </td>


                                    <td class=" justify-center">
                                        <div>
                                            <a href="{{ route('admin.recetas-edit', ['row' => $row->id]) }}">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mx-auto">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10" />
                                                </svg>
                                            </a>
                                        </div>

                                        <div>
                                            <a href="{{ route('admin.recetas-view', ['row' => $row->id]) }}">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mx-auto">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M2.036 12.322a1.012 1.012 0 0 1 0-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178Z" />
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z" />
                                                </svg>
                                            </a>
                                        </div>


                                        <button wire:click="delete({{ $row->id }})" wire:confirm.prompt="¿ESTA SEGURO?,escribe INACTIVO|INACTIVO">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6 mx-6 md:mx-10">
                                                <path stroke-linecap="round" stroke-linejoin="round" d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0" />
                                            </svg>

                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <div class="flex items-center mt-6 text-center border rounded-lg h-96 dark:border-gray-700">
                                    <div class="flex flex-col w-full max-w-sm px-4 mx-auto">
                                        <div class="p-3 mx-auto text-blue-500 bg-blue-100 rounded-full dark:bg-gray-800">
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                                <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                                            </svg>
                                        </div>
                                        <h1 class="mt-3 text-lg text-gray-800 dark:text-white">Sin resultados</h1>
                                        <p class="mt-2 text-gray-500 dark:text-gray-400">Tu consulta no arroja resultado, escribe bien!</p>
                                        <div class="flex items-center mt-4 sm:mx-auto gap-x-3">
                                            <button wire:click="clean" class="flex items-center justify-center w-1/2 px-5 py-2 text-sm text-gray-700 transition-colors duration-200 bg-white border rounded-lg gap-x-2 sm:w-auto dark:hover:bg-gray-800 dark:bg-gray-900 hover:bg-gray-100 dark:text-gray-200 dark:border-gray-700">
                                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0 3.181 3.183a8.25 8.25 0 0 0 13.803-3.7M4.031 9.865a8.25 8.25 0 0 1 13.803-3.7l3.181 3.182m0-4.991v4.99" />
                                                </svg>
                                            </button>

                                            @livewire('aap-otro-rec')

                                        </div>
                                    </div>
                                </div>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="mt-6 sm:flex sm:items-center sm:justify-between ">
            <div class="flex items-center mt-4 gap-x-4 sm:mt-0">
                @if(!empty($recetas))
                {{ $recetas->links() }}
                @endif
            </div>
        </div>
    </section>

</div>