<div>
@role('admin')                                        

    <section class="container px-4 mx-auto">

        <div class="flex flex-col mt-6">
            <div class="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                    <div class="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg">

                    <div class="grid grid-cols-3 gap-8 h-12 hidden sm:grid " style="background:#FB718B;">
                            <div><p class="sm:m-5  sm:mt-3">Nombre Usuario</p></div>
                            <div class="sm:m-5  sm:mt-3">Mensaje</div>
                            <div></div>
                        </div>

                        <div class="grid grid-cols-3 gap-8 h-12 sm:hidden" style="background:#FB718B;">
                            <div><p class="m-5 mt-3">Buzon</p></div>
                            <div></div>
                        </div>
                        @if ($com && count($com) > 0)

                         @foreach($com as $row)

                        <div class="grid md:grid-cols-3 gap-8">
                           <div><p class="m-5 text-center">{{$row->name}}</p></div>
                           <div><textarea class="text-black w-11/12 m-1 rounded-md" disabled cols="30" rows="10">{{$row->mensaje}}</textarea></div>
                           <div>
                            
                           <form method="POST" action="{{ route('admin.response') }}">
                                            @csrf
                                            <textarea maxlength="100" id="repost" name="repost" rows="4" class="block p-2.5 m-1 text-sm text-gray-900 bg-gray-50 
                                      rounded-lg border border-gray-300 focus:ring-blue-500 
                                      focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 
                                      dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 
                                      dark:focus:border-blue-500 w-11/12" placeholder="Escribe aqui tu respuesta"></textarea>


                                            <input type="hidden" name="id" value="{{$row->id}}">
                                            <button type="submit" class=" mt-1 py-2 px-3 hover:bg-pink-400 rounded-md uppercase text-white ml-2" style="background:#FB718B;">Responder</button>
                                        </form>
                           </div>
                        </div>


                                @endforeach
                            @else
                            <p>
                                sin data
                            </p>
                            @endif


                    </div>
                </div>
            </div>
        </div>

        <div class="flex items-center justify-between mt-6">
            @if(!empty($com))
            {{ $com->links() }}
            @endif
        </div>
    </section>
@endrole


@role('cliente')                                        
    <section class="container px-4 mx-auto">

        <div class="flex flex-col mt-6">
            <div class="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                    <div class="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg">




                    <div class="grid grid-cols-3 gap-8 h-12 hidden sm:grid " style="background:#FB718B;">
                            <div><p class="sm:m-5  sm:mt-3">Nombre Usuario</p></div>
                            <div class="sm:m-5  sm:mt-3">Mensaje</div>
                            <div></div>
                        </div>

                        <div class="grid grid-cols-3 gap-8 h-12 sm:hidden" style="background:#FB718B;">
                            <div><p class="m-5 mt-3">Buzon</p></div>
                            <div></div>
                        </div>

                            @if ($cliente && count($cliente) > 0)

                                @foreach($cliente as $row)

                                <div class="grid md:grid-cols-3 gap-8">

                                        <div class="">

                                        <div><p class="m-5 text-center">{{$row->name}}</p></div>
                                        </div>

                                        <textarea class="text-black mt-2" disabled cols="30" rows="10">{{$row->mensaje}}</textarea>


                                        <textarea maxlength="100" rows="4" class="block p-2.5 mt-2 w-full text-md text-black bg-gray-50 
                                      rounded-lg border border-gray-300 focus:ring-blue-500 
                                      focus:border-blue-500" disabled>{{$row->mensaje_admin}}</textarea>

                            
                                      </div>
                                @endforeach

                            @else
                            <p>Sin data</p>
                            @endif

                    </div>
                </div>
            </div>
        </div>

        <div class="flex items-center justify-between mt-6">
            @if(!empty($com))
            {{ $com->links() }}
            @endif
        </div>
    </section>
    @endrole

</div>