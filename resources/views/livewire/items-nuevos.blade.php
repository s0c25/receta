<div>
    <button wire:click="$set('open',true)" class="flex items-center justify-center p-2 mx-1 text-sm tracking-wide text-white transition-colors duration-200 rounded-lg shrink-0 sm:w-auto gap-x-2 bg-black">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span>Agregar <br> Items</span>
    </button>

    <x-dialog-modal wire:model.live="open">

        <x-slot name="title">Rellenar todos los para la receta</x-slot>
        <x-slot name="content">

            <x-validation-errors class="mb-4" />

            <x-form-section submit="save_ingrediente">
                <x-slot name="title">
                    {{ __('Nuevos Ingredientes') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('Puedes agregar ingredientes nuevas.') }}
                </x-slot>

                <x-slot name="form">
                    <div class="col-span-6 sm:col-span-4">
                        <x-label for="ingrediente" value="{{ __('Nombre Ingrediente') }}" />
                        <x-input id="ingrediente" type="text" class="mt-1 block w-full" wire:model="state.ingrediente" autocomplete="ingrediente" />
                        <x-input-error for="ingrediente" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="actions">
                    <x-action-message class="me-3" on="saved">
                        {{ __('Saved.') }}
                    </x-action-message>

                    <x-button>
                        {{ __('Save') }}
                    </x-button>
                </x-slot>
            </x-form-section>

            <x-form-section submit="save_medida">
                <x-slot name="title">
                    {{ __('Nueva Medida') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('Puedes agregar  medidas nuevas.') }}
                </x-slot>

                <x-slot name="form">
                    <div class="col-span-6 sm:col-span-4">
                        <x-label for="medida" value="{{ __('Nombre Medida') }}" />
                        <x-input id="medida" type="text" class="mt-1 block w-full" wire:model="state.medida" autocomplete="medida" />
                        <x-input-error for="medida" class="mt-2" />
                    </div>
                </x-slot>

                <x-slot name="actions">
                    <x-action-message class="me-3" on="savedd">
                        {{ __('Saved.') }}
                    </x-action-message>

                    <x-button>
                        {{ __('Save') }}
                    </x-button>
                </x-slot>
            </x-form-section>


        </x-slot>
        <x-slot name="footer">
            <button type="button" wire:click="cerrar" class="p-4 py-2  rounded-md bg-black text-blod text-white">Cerrar</button>
        </x-slot>
        </form>

    </x-dialog-modal>
</div>