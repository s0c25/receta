<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Vencido') }}
        </h2>
    </x-slot>

    <div class=" lg:gap-8 p-6 lg:p-8">

        <section class="text-gray-400 body-font ">
            <div class="container">
                <div class="lg:w-2/3 w-full mx-auto ">

                    <section class="bg-white dark:bg-gray-900">
                        <div class="container flex flex-col items-center px-4 py-12 mx-auto text-center">
                            <h2 class="max-w-2xl mx-auto text-2xl font-semibold tracking-tight text-gray-800 xl:text-3xl dark:text-white">
                                Tienes planificaciones <span class="text-blue-500">por vencer</span>
                            </h2>
                            @foreach ($resultados as $row)

                            <div class="w-full mt-6 sm:w-auto text-center">

                                <p class="max-w-4xl mt-2 text-black dark:text-black">
                                    <span class="font-bold">Nombre Planificación:</span> {{ $row->nombre }}
                                </p>
                                <p class="max-w-4xl mt-2 text-black dark:text-black">
                                    <span class="font-bold">Fecha Fin Planificación:</span> {{ $row->fechafin }}
                                </p>
                                <a href="{{ route('cliente.editar-plani', ['row' => $row->id]) }}" 
                                class="inline-flex items-center justify-center w-full px-6 py-2 mt-4 
                                text-black duration-300 bg-blue-600 rounded-lg hover:bg-blue-500 
                                focus:ring focus:ring-blue-300 focus:ring-opacity-80">
                                    Ver
                                </a>
                            </div>

                            @endforeach
                        </div>
                    </section>

                </div>
            </div>
        </section>
    </div>
    @include('sweetalert::alert')
</x-app-layout>