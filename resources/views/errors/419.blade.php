@extends('errors::minimal')

@section('title', __('Page Expired'))
@section('code', '419')
@section('message', __('Page Expired'))


<script>
    // Redirigir a la página principal después de 3000 milisegundos (3 segundos)
    setTimeout(function () {
        window.location.href = '/';
    }, 3000);
</script>