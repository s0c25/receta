<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" x-data :class="$store.darkMode.on && 'dark'">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Receta</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />
    <link rel="apple-touch-icon" href="{{ asset('logo.svg') }}">

    <link rel="manifest" href="{{ asset('/manifest.json') }}">
    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])


</head>

<body class="font-sans antialiased">
    <section class="max-w-2xl px-6 py-8 mx-auto bg-white dark:bg-gray-900">
        <header>
            <a href="#">
                <img class="w-auto h-7 sm:h-8" src="{{ asset('storage/' . $idReceta->foto_receta) }}" alt="">
            </a>

        </header>

        <main class="mt-8">

            <h2 class="mt-6 text-gray-700 dark:text-gray-200">{{$idReceta->nombre_receta}}</h2>

            <p class="mt-2 leading-loose text-gray-600 dark:text-gray-300">
                Preparación: {{$idReceta->preparacion}}
            </p>
            <p class="mt-2 leading-loose text-gray-600 dark:text-gray-300">
                Tiempo Preparacipon: {{$idReceta->tiempo_preparacion}}
            </p>
            <p class="mt-2 leading-loose text-gray-600 dark:text-gray-300">
                Porciones: {{$idReceta->porciones}}
            </p>
            <p class="mt-2 leading-loose text-gray-600 dark:text-gray-300">
                Categoria: {{$idReceta->categoria->nombre_categoria}}
            </p>
            <p class="mt-2 leading-loose text-gray-600 dark:text-gray-300">
                Calorías: {{$idReceta->categoria->calorias}}
            </p>




            <table>
                <thead>
                    <tr>
                        <th>Ingredientes</th>
                        <th>Medida</th>
                        <th>Cantidad</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($datas as $row)
                    <tr>
                        <td>
                        {{$row->nombre_ingrediente}}
                        </td>
                        <td>
                        {{$row->nombre_unidad}}
                        </td>
                        <td>
                        {{$row->cantidad}}
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>

        </main>


    </section>
</body>

</html>