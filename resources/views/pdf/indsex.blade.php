<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Horario de Comidas</title>
  <style>
    .titulo {
      text-transform: capitalize;
      margin: 1rem;
    }

    .centro {
      justify-content: center;
      text-align: center;
    }

    table {
      border-style: solid;
      border: 5px;
      border-color: red;
      border-collapse: collapse;
      border-radius: 10px;
      overflow: hidden;
      width: 80%;
      margin: 20px auto;
    }

    th,
    td {
      padding: 15px;
      text-align: center;

    }

    th {
      background-color: #3498db;
      /* Azul */
      color: #fff;

    }

    td.breakfast {
      background-color: #e91e63;
      /* Rosado */
      color: #fff;

    }

    td.lunch {
      background-color: #e91e63;
      /* Rosado */
      color: #fff;
    }

    td.dinner {
      background-color: #e91e63;
      /* Rosado */
      color: #fff;

    }
  </style>
</head>

<h1 class="centro titulo ">
  Plan de comidas de dietas saludables
</h1>

<table>
  <thead>
    <tr>
      <th></th>
      <th>Lunes</th>
      <th>Martes</th>
      <th>Miércoles</th>
      <th>Jueves</th>
      <th>Viernes</th>
      <th>Sabado</th>
      <th>Domingo</th>

    </tr>
  </thead>
  <tbody>
    @php
    $totalCalorias = 0;

    foreach ($data as $detalle) {
    // Asegúrate de tener la propiedad 'calorias' en el objeto
    $calorias = $detalle->calorias ?? 0;

    // Sumar las calorías al total
    $totalCalorias += (int) $calorias;
    }
    @endphp
    @foreach($data as $row)

    <tr>
      <td class="breakfast">Breakfast</td>
      <td>
        @if($row->nombre_dia == 'Lunes')
        {{ $row->nombre_receta }}
        @endif
      </td>
      <td>
        @if($row->nombre_dia == 'Martes')
        {{ $row->nombre_receta }}
        @endif
      </td>
      <td>
        @if($row->nombre_dia == 'Miercoles')
        {{ $row->nombre_receta }}
        @endif
      </td>
      <td>
        @if($row->nombre_dia == 'Jueves')
        {{ $row->nombre_receta }}
        @endif
      </td>
      <td>
        @if($row->nombre_dia == 'Viernes')
        {{ $row->nombre_receta }}
        @endif
      </td>
      <td>
        @if($row->nombre_dia == 'Sabado')
        {{ $row->nombre_receta }}
        @endif
      </td>
      <td>
        @if($row->nombre_dia == 'Domingo')
        {{ $row->nombre_receta }}
        @endif
      </td>

    </tr>
    @endforeach
    <tr>
      <td class="dinner">Total Calories: {{$totalCalorias}}</td>
    </tr>
  </tbody>
</table>
</body>

</html>