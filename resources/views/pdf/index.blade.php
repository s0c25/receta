<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Horario de Comidas</title>
  <style>
    .titulo {
      text-transform: capitalize;
      margin: 1rem;
    }

    .centro {
      justify-content: center;
      text-align: center;
    }

    table {
      border-collapse: collapse;
      border-radius: 10px;
      overflow: hidden;
      width: 80%;
      margin: 20px auto;
      border: 2px solid #3498db;
      /* Azul */
    }

    th,
    td {
      padding: 15px;
      text-align: center;
      border: 1px solid #ccc;
      /* Color de borde gris claro */
    }

    th {
      background-color: #3498db;
      /* Azul */
      color: #fff;

    }

    td.breakfast {
      background-color: #e91e63;
      /* Rosado */
      color: #fff;

    }

    td.lunch {
      background-color: #e91e63;
      /* Rosado */
      color: #fff;
    }

    td.dinner {
      background-color: #e91e63;
      /* Rosado */
      color: #fff;

    }
  </style>
</head>

<h1 class="centro titulo ">
  Plan de comidas
</h1>

<div class="flex flex-col ">
  <div class="justify end">
    @php
    $totalCalorias = 0;

    foreach ($data as $detalle) {
    // Asegúrate de tener la propiedad 'calorias' en el objeto
    $calorias = $detalle->calorias ?? 0;

    // Sumar las calorías al total
    $totalCalorias += (int) $calorias;
    }
    @endphp
    <div>
      <p>Total Calories: {{$totalCalorias}}</p>
      <p>Fecha Inicio {{$wa->fechainicio}}</p>
      <p>Fecha Fin: {{$wa->fechafin}}</p>
    </div>

  </div>
</div>
<div class="w-full overflow-x-auto">
<table class="w-full border border-black">
    <thead>
        <tr>
            <th class="border"></th>
            <th class="border">Lunes</th>
            <th class="border">Martes</th>
            <th class="border">Miércoles</th>
            <th class="border">Jueves</th>
            <th class="border">Viernes</th>
            <th class="border">Sábado</th>
            <th class="border">Domingo</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="border breakfast">Recetas</td>
            <td class="border">
                @foreach ($dialunes as $receta)
                    {{ $receta['nombre_receta'] }}
                    {{ \Carbon\Carbon::parse($receta['hora_comida'])->format('H:i') }}
    <hr style="border-top: 1px solid #ccc; margin: 5px 0;">
                @endforeach
            </td>
            <td class="border">
                @foreach ($martes as $receta)
                    {{ $receta['nombre_receta'] }} 
                    {{ \Carbon\Carbon::parse($receta['hora_comida'])->format('H:i') }}
    <hr style="border-top: 1px solid #ccc; margin: 5px 0;">
                @endforeach
            </td>
            <td class="border">
                @foreach ($miercoles as $receta)
                    {{ $receta['nombre_receta'] }}  
                    {{ \Carbon\Carbon::parse($receta['hora_comida'])->format('H:i') }}
    <hr style="border-top: 1px solid #ccc; margin: 5px 0;">
                @endforeach
            </td>
            <td class="border">
                @foreach ($jueves as $receta)
                    {{ $receta['nombre_receta'] }}  
                    {{ \Carbon\Carbon::parse($receta['hora_comida'])->format('H:i') }}
    <hr style="border-top: 1px solid #ccc; margin: 5px 0;">
                @endforeach
            </td>
            <td class="border">
                @foreach ($viernes as $receta)
                    {{ $receta['nombre_receta'] }}   
                    {{ \Carbon\Carbon::parse($receta['hora_comida'])->format('H:i') }}
    <hr style="border-top: 1px solid #ccc; margin: 5px 0;">
                @endforeach
            </td>
            <td class="border">
                @foreach ($sabado as $receta)
                    {{ $receta['nombre_receta'] }}  
                    {{ \Carbon\Carbon::parse($receta['hora_comida'])->format('H:i') }}
    <hr style="border-top: 1px solid #ccc; margin: 5px 0;">
                @endforeach
            </td>
            <td class="border">
                @foreach ($domingo as $receta)
                    {{ $receta['nombre_receta'] }}   
                    {{ \Carbon\Carbon::parse($receta['hora_comida'])->format('H:i') }}
    <hr style="border-top: 1px solid #ccc; margin: 5px 0;">
                @endforeach
            </td>
        </tr>
    </tbody>
</table>




</div>

</body>

</html>