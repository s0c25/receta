     <!-- aqui te voy a poner el carusel  mamaguevo pendejo -->
 


     <!-- fin de carusel pendejo maricon  -->


  <style>

    p{
        margin:0;
        text-align:right;
    }
    .carousel-container {
      position: relative;
      max-width: 600px; /* Adjust the max-width as per your requirement */
      overflow: hidden;
      border-radius: 10px;
    }

    .carousel {
      display: flex;
      transition: transform 0.5s ease-in-out;
    }

    .carousel-item {
      position: relative;
      width: calc(90% / 3); /* Set the width to 1/3 of the container */
      margin: 4px;
    }

    .carousel img {
      width: 100%;
      border-radius: 10px;
    }

    .transparent-banner {
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: rgba(255,255,255,0.5);
      color: black;
      padding: 1px;
      border-bottom-left-radius: 10px;
      border-bottom-right-radius: 10px;
    }

    .control-btn {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      width: 40px;
      height: 40px;
      background-color: #e63244;
      color: #fff;
      font-size: 20px;
      text-align: center;
      line-height: 40px;
      cursor: pointer;
      border-radius: 50%;
    }

    .prev-btn {
      left: 10px;
    }

    .next-btn {
      right: 10px;
    }
  </style>
</head>
<body>

<div class="carousel-container">
  <div class="carousel">
    <div class="carousel-item">
    <img src="{{URL::asset('https://www.comedera.com/wp-content/uploads/2019/11/arroz-blanco-cocido.jpg')}}">
      <div class="transparent-banner">
        <p>30c</p>
        <p>15-10 min</p>
      </div>
    </div>
    <div class="carousel-item">
    <img src="{{URL::asset('https://i.blogs.es/a75550/nuevo_arroz_blanco/840_560.jpg')}}">
      <div class="transparent-banner">
        <p>30c</p>
        <p>15-10 min</p>
      </div>
    </div>
    <div class="carousel-item">
    <img src="{{URL::asset('https://www.naturalcastello.com/wp-content/uploads/2019/08/propiedades-arroz-750x448.jpg')}}">
      <div class="transparent-banner">
        <p>30c</p>
        <p>15-10 min</p>
      </div>
    </div>
    <!-- Add more images as needed -->
  </div>
  <div class="control-btn prev-btn" onclick="prevSlide()">❮</div>
  <div class="control-btn next-btn" onclick="nextSlide()">❯</div>
</div>

<script>
  let currentIndex = 0;
  const totalSlides = document.querySelectorAll('.carousel-item').length;

  function updateCarousel() {
    document.querySelector('.carousel').style.transform = `translateX(${-currentIndex * (100 / 3)}%)`;
  }

  function nextSlide() {
    currentIndex = (currentIndex + 1) % totalSlides;
    updateCarousel();
  }

  function prevSlide() {
    currentIndex = (currentIndex - 1 + totalSlides) % totalSlides;
    updateCarousel();
  }
</script>

</body>
