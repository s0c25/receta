<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Recetas Disponibles') }}
        </h2>
    </x-slot>

    <div wire:offline.class="bg-red-300" class=" lg:gap-8 p-6 lg:p-8">

        <section class="text-gray-400 body-font ">
            <div class="container">
                <div class="lg:w-2/3 w-full mx-auto ">
                    <div class="max-w-2xl overflow-hidden bg-white rounded-lg shadow-md dark:bg-gray-800">
                        <img class="object-cover w-full h-64" src="{{ asset('storage/' . $idReceta->foto_receta) }}" alt="Article">

                        <div class="p-6">
                            <div>
                            <p class="mt-2 text-xl text-gray-600 dark:text-gray-400">
                                    <a href="{{ route('cliente.favorito', ['id' => $idReceta->id]) }}" id="favorito"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="M11.48 3.499a.562.562 0 0 1 1.04 0l2.125 5.111a.563.563 0 0 0 .475.345l5.518.442c.499.04.701.663.321.988l-4.204 3.602a.563.563 0 0 0-.182.557l1.285 5.385a.562.562 0 0 1-.84.61l-4.725-2.885a.562.562 0 0 0-.586 0L6.982 20.54a.562.562 0 0 1-.84-.61l1.285-5.386a.562.562 0 0 0-.182-.557l-4.204-3.602a.562.562 0 0 1 .321-.988l5.518-.442a.563.563 0 0 0 .475-.345L11.48 3.5Z" />
</svg>
</a>
                                </p>
                                <span class="text-xs font-medium text-blue-600 uppercase dark:text-blue-400">Cantidad de Personas: {{$idReceta->porciones }}</span>
                                <a href="#" class="block mt-2 text-xl font-semibold text-gray-800 transition-colors duration-300 transform dark:text-white hover:text-gray-600 hover:underline" tabindex="0" role="link">{{$idReceta->nombre_receta}} </a>
                                <p class="mt-2 text-sm text-gray-600 dark:text-gray-400">Calorías:{{$idReceta->calorias}}</p>
                                <p class="mt-2 text-sm text-gray-600 dark:text-gray-400">Categoría:{{$idReceta->categoria->nombre_categoria}}</p>
                                <p class="mt-2 text-xl text-gray-600 dark:text-gray-400">{{$idReceta->preparacion }}</p>
                                

                            </div>
                            <div>
                                <div class="space-y-4">
                                    <details class="group rounded-lg bg-gray-50 p-6 [&_summary::-webkit-details-marker]:hidden" open>
                                        <summary class="flex cursor-pointer items-center justify-between gap-1.5 text-gray-900">
                                            <h2 class="font-medium">Tiempo Preparación</h2>

                                            <span class="relative h-5 w-5 shrink-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute inset-0 h-5 w-5 opacity-100 group-open:opacity-0" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>

                                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute inset-0 h-5 w-5 opacity-0 group-open:opacity-100" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                            </span>
                                        </summary>

                                        <p class="mt-4 leading-relaxed text-gray-700">
                                            {{$idReceta->tiempo_preparacion}}
                                        </p>
                                    </details>

                                    <!-- <details class="group rounded-lg bg-gray-50 p-6 [&_summary::-webkit-details-marker]:hidden">
                                        <summary class="flex cursor-pointer items-center justify-between gap-1.5 text-gray-900">
                                            <h2 class="font-medium">Porciones</h2>

                                            <span class="relative h-5 w-5 shrink-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute inset-0 opacity-100 group-open:opacity-0" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>

                                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute inset-0 opacity-0 group-open:opacity-100" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                            </span>
                                        </summary>

                                        <p class="mt-4 leading-relaxed text-gray-700">
                                            {{$idReceta->porciones}}
                                        </p> 
                                    </details> -->

                                    <details class="group rounded-lg bg-gray-50 p-6 [&_summary::-webkit-details-marker]:hidden" open>
                                        <summary class="flex cursor-pointer items-center justify-between gap-1.5 text-gray-900">
                                            <h2 class="font-medium">Ingredientes y medidas</h2>

                                            <span class="relative h-5 w-5 shrink-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute inset-0 h-5 w-5 opacity-100 group-open:opacity-0" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>

                                                <svg xmlns="http://www.w3.org/2000/svg" class="absolute inset-0 h-5 w-5 opacity-0 group-open:opacity-100" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                            </span>
                                        </summary>

                                        <div class="overflow-x-auto">
                                            <table class="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
                                                <thead class="ltr:text-left rtl:text-right">
                                                    <tr>
                                                        <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900 rounded-ss-md">Ingredientes</th>
                                                        <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">Medidas</th>
                                                        <th class="whitespace-nowrap px-4 py-2 font-medium text-gray-900 rounded-se-md">Cantidad</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="divide-y divide-gray-200">
                                                    @if ($data && count($data) > 0)
                                                    @foreach($data as $row)
                                                    <tr>

                                                        <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">{{$row->nombre_ingrediente}}</td>
                                                        <td class="whitespace-nowrap px-4 py-2 text-gray-700">{{$row->nombre_unidad}}</td>
                                                        <td class="whitespace-nowrap px-4 py-2 text-gray-700">{{$row->cantidad}}</td>

                                                    </tr>
                                                    @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </details>
                                </div>
                            </div>

                            <div class="mt-4">
                                <div class="flex items-center">
                                    <div class="flex items-center">
                                        <img class="object-cover h-10 rounded-full" src="{{ asset('storage/' . $adminUser->profile_photo_path) }}" alt="Avatar">
                                        <a href="#" class="mx-4 font-semibold text-gray-700 dark:text-gray-200" tabindex="0" role="link">Chef. {{ $adminUser->name }} </a>
                                    </div>
                                    </br>
                                    <span class="mx-4 text-xs text-gray-600 dark:text-gray-300">{{ \Carbon\Carbon::parse($adminUser->created_at)->format('d F Y') }}</span>
                                </div>

                            </div>
                        </div>
                        <div class="flex">

                            <a href="{{ route('anterior.pagina-cliente', ['id' => $idReceta->id]) }}" class="flex items-center px-4 py-2 mx-1 text-gray-500 bg-white rounded-md cursor-not-allowed dark:bg-gray-800 dark:text-gray-600">
                                Anterior
                            </a>
                            <a href="{{ route('siguiente.pagina-cliente', ['id' => $idReceta->id]) }}" class="flex items-center px-4 py-2 mx-1 text-gray-700 transition-colors duration-300 transform bg-white rounded-md dark:bg-gray-800 dark:text-gray-200 hover:bg-blue-600 dark:hover:bg-blue-500 hover:text-white dark:hover:text-gray-200" onclick="cambiarPagina(1)">
                                Siguiente
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            @include('sweetalert::alert')

        </section>

    </div>
    
    <script>
    let touchStartX = 0;
    let touchEndX = 0;
    let lastTouchEnd = 0;

    document.addEventListener('touchstart', (e) => {
        touchStartX = e.touches[0].clientX;
    });

    document.addEventListener('touchmove', (e) => {
        touchEndX = e.touches[0].clientX;
    });

    document.addEventListener('touchend', (e) => {
        const currentTime = new Date().getTime();
        const tapLength = currentTime - lastTouchEnd;

        // Verifica si el toque ocurrió en la parte inferior de la pantalla
        const windowHeight = window.innerHeight;
        const touchY = e.changedTouches[0].clientY;

        if (touchY >= windowHeight * 0.8) { // Ajusta el valor según sea necesario
            handleSwipe();
        }

        // Detección de doble toque
        if (tapLength < 500 && tapLength > 0) {
            handleDoubleTap();
        }

        lastTouchEnd = currentTime;
    });

    function handleSwipe() {
        const swipeThreshold = 50;
        const deltaX = touchEndX - touchStartX;

        if (deltaX > swipeThreshold) {
            window.location.href = "{{ route('anterior.pagina-cliente', ['id' => $idReceta->id]) }}";
        } else if (deltaX < -swipeThreshold) {
            window.location.href = "{{ route('siguiente.pagina-cliente', ['id' => $idReceta->id]) }}";
        }
    }

    function handleDoubleTap() {
        // Ejecutar acción de favorito
        document.getElementById('favorito').click();
    }
</script>



    @include('sweetalert::alert')
</x-app-layout>