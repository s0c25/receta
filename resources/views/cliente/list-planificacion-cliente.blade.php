<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Cliente Personalizar Planificación ') }}
        </h2>
    </x-slot>

    <div wire:offline.class="bg-red-300" class=" lg:gap-8 p-6 lg:p-8">

        <section class="text-gray-400 body-font ">

            <div class="container">
                <div class="lg:w-2/3 w-full mx-auto ">
                    <div class="max-w-2xl overflow-hidden bg-white rounded-lg shadow-md dark:bg-gray-800">
                        <img class="object-cover w-full h-64" src="{{ asset('storage/' . $idPlanificacion->foto_planificacion) }}" alt="Article">
                        <form method="POST" action="{{ route('guardar.planificacion') }}">
                            @csrf

                            @if($fechasUnicas == 'null')
                            <label for="fechainicio">Fecha Inicio {{$ise->fechainicio}} - Fecha Fin {{$ise->fechafin}}</label>
                            @else
                            @foreach($fechasUnicas as $fecha => $valor)
                            <label for="fechainicio">Fecha Inicio {{$valor}} - Fecha Fin {{$fecha}}</label>
                            @endforeach
                            @endif
                            <div class="devide-y-2">
                                <label for="fechafin">Fecha Inicio</label>
                                <input id="fechainicio" class="w-auto" type="date" name="fechainicio">
                                <label for="fechafin">Fecha Fin</label>
                                <input id="fechafin" class="w-auto" type="date" name="fechafin">
                            </div>


                            <div class="p-6">
                                <div>
                                    <div class="space-y-4">
                                        <div class="">
                                            <div class="container">
                                                <div class="grid grid-cols-2 gap-2 ">
                                                    <div><span class="text-black text-xs sm:text-sm">Nombre Planificacion: <input class="bg-gray-100 w-11/12" readonly="readonly" disabled value="{{$idPlanificacion->nombre}}" type="text"></span><input type="hidden" value="{{$idPlanificacion->slug}}" name="slug"></div>

                                                </div>
                        </form>
                        <div class="flex flex-wrap ">
                            <div style="overflow-y: auto;">

                                <table id="tablaplanificacionactivos" class="min-w-full divide-y-2 divide-gray-200 bg-white text-sm">
                                    <thead class="ltr:text-left rtl:text-right">
                                        <tr>
                                            <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900 rounded-ss-md">Nombre receta </th>
                                            <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">Categoria</th>
                                            <th class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900 rounded-se-md">Fecha Receta</th>
                                            

                                        </tr>
                                    </thead>

                                    @role('cliente')
                                    <tbody class="divide-y divide-gray-200">
                                        @if ($rows && count($rows) > 0)
                                        @foreach($rows as $row)
                                        @php
                                        $fecha = \Carbon\Carbon::parse($row->hora_comida);
                                        $fecha->setLocale('es');
                                        $nombreDia = $row->nombre_dia; // Obtén el nombre completo del día de la semana en español
                                        $hora = $fecha->format('H:i');
                                        $fechaInicio = \Carbon\Carbon::parse($row->detalle_planificacion_usuariosfechainicio);
                                        $fechaFin = \Carbon\Carbon::parse($row->detalle_planificacion_usuariosfechafin);
                                        @endphp

                                        <tr>
                                            <td class="whitespace-nowrap px-4 py-2 font-medium text-gray-900">{{$row->nombre_receta}} - {{$row->calorias  ?? 0}}</td>
                                            <td class="whitespace-nowrap sm:px-4 sm:py-2 font-medium text-gray-900">{{$row->nombre_categoria}}</td>
                                            <td class="whitespace-nowrap px-4 py-2 text-gray-700">{{$nombreDia}} - {{$hora}}</td>
                                            <td class="whitespace-nowrap px-4 py-2 text-gray-700"><a class="text-red-600 hover:underline" onclick="eliminarDato('{{ $row->id }}')">X</a></td>

                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                    @endrole

                                </table>
                            </div>
                        </div>

                        <div class="">
                            @role('cliente')
                            <div id="formularioAgregar-cliente">
                                <input type="hidden" name="idPlanificacion" value="{{ $idPlanificacion->id }}">

                                <label for="">Cambiar Fecha receta </label>
                                <button class="bg-black rounded-md p-2 m-2 text-white" type="button" onclick="agregarFila_cliente()">Agregar Fila</button>

                                <div class="flex flex-row gap-2 mt-2">
                                    <div class="divide-y-2">
                                        <select name="recetas[]" class="recetasSelect w-11/12" onchange="validarFormularioCliente()">
                                            @foreach ($recetasall as $row)

                                            <option selected value="{{ $row->id }}">{{ $row->nombre_receta}}</option>

                                            @endforeach
                                        </select>
                                    </div>


                                    <select name="dias[]" class="Dias w-11/12" onchange="validarFormularioCliente()">
                                        @foreach ($dias as $row)
                                        <option selected value="{{ $row->id }}">{{ $row->nombre}}</option>
                                        @endforeach
                                    </select>
                                    <input class="w-11/12"  type="time" name="hora_inicio[]" onchange="validarFormularioCliente()">
                                </div>
                            </div>

                            <div class="flex flex-wrap mt-2">
                                <button type="submit" class="bg-black rounded-md p-2 m-2 text-white" id="guardarButton" disabled>Guardar</button>
                            </div>
                            @endrole

                        </div>
                        </form>

                    </div>
                </div>
            </div>
    </div>
    </div>
    </div>
    </div>
    </section>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

    <script>
        function eliminarDato(id) {
            const url = '{{ route("cliente.eliminar.datos-planificacion", ["id" => ":id"]) }}';
            const apiUrl = url.replace(':id', id);
            const confirmacion = confirm('¿Estás seguro de que deseas eliminar este ingrediente?');
            axios.delete(apiUrl)
                .then(function(response) {
                    console.log(response.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Datos guardados con éxito',
                    });
                    actualizarSeccion();
                })
                .catch(function(error) {
                    console.error(error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al guardar los datos',
                    });
                });
        }

        function actualizarSeccion() {
            console.log('Se está llamando a actualizarSeccion');
            $('#tablaplanificacionactivos').load(location.href + ' #tablaplanificacionactivos', function() {
                console.log('Sección actualizada con éxito');


            });
        }
    </script>

    <script>
        // Obtener referencias a los elementos HTML
        var inputfechainicio = document.getElementById('fechainicio');
        var inputfechafin = document.getElementById('fechafin');
        var guardarButton = document.getElementById('guardarButton');

        // Función para verificar y actualizar el estado del botón
        function actualizarEstadoBoton() {
            // Verificar si ambos campos de fecha están llenos
            var fechainicioLleno = inputfechainicio.value.trim() !== '';
            var fechafinLleno = inputfechafin.value.trim() !== '';

            // Habilitar o deshabilitar el botón según el estado de los campos de fecha
            guardarButton.disabled = !(fechainicioLleno && fechafinLleno);
        }

        // Agregar escuchadores de eventos para los campos de fecha
        inputfechainicio.addEventListener('input', actualizarEstadoBoton);
        inputfechafin.addEventListener('input', actualizarEstadoBoton);
    </script>

    <script>
        function validarFormularioCliente() {
            var recetasSelect = document.querySelector('.recetasSelect');
            var diasSelect = document.querySelector('.Dias');
            var horaInicioInput = document.querySelector('input[name="hora_inicio[]"]');
            var guardarButton = document.getElementById('guardarButton');

            // var isValid = fechafinInput.value.trim() !== '' && fechainicioInput.value.trim() !== '' && horaInicioInput.value.trim() !== '' && Dias.value.trim() !== '' && recetasSelect.value.trim() !== '';
            // guardarButton.disabled = !isValid;

            console.log('Recetas:', recetasSelect.value);
            console.log('Días:', diasSelect.value);
            console.log('Hora Inicio:', horaInicioInput.value);

            // Verificar si todos los campos tienen datos
            if (recetasSelect.value.length !== 0 && diasSelect.value.length !== 0 && horaInicioInput.value.length !== 0) {
                console.log('Habilitar botón');
                guardarButton.disabled = false; // Habilitar el botón
            } else {
                console.log('Deshabilitar botón');
                guardarButton.disabled = true; // Deshabilitar el botón si algún campo está vacío
            }
        }
    </script>
    <script>
        function agregarFila_cliente() {
            const nuevaFila = document.createElement('div');
            nuevaFila.className = 'grid grid-cols-3';
            nuevaFila.innerHTML = `
            <div class="divide-y-2">
            <select name="recetas[]" class="recetasSelect w-11/12" onchange="validarFormularioCliente()">
                                                                @foreach ($recetasall as $row)

                                                                <option value="{{ $row->id }}">{{ $row->nombre_receta}}</option>

                                                                @endforeach
                                                            </select>
            </div>

            <select name="dias[]" class="Dias w-11/12" onchange="validarFormularioCliente()">
                                                                @foreach ($dias as $row)
                                                                <option value="{{ $row->id }}">{{ $row->nombre}}</option>
                                                                @endforeach
                                                            </select>                                                        


            <div class="divide-y-2">
                <input class="w-11/12"  type="time" name="hora_inicio[]"  onchange="validarFormularioCliente()">
            </div>
            <button type="button" onclick="eliminarFila(this)">Eliminar Fila</button>

        `;

            document.getElementById('formularioAgregar-cliente').appendChild(nuevaFila);

        }

        function eliminarFila(boton) {
            const fila = boton.parentNode;
            fila.parentNode.removeChild(fila);
        }

        // function guardarDatos_cliente() {

        //     // Crear un objeto FormData
        //     const formData = new FormData(document.getElementById('formularioAgregar-cliente'));
        //     var inputfechainicio = document.getElementById('fechainicio');

        //     var inputfechafin = document.getElementById('fechafin');

        //     var fechainicio = inputfechainicio.value;
        //     var fechafin = inputfechafin.value;
        //     formData.append('fechainicio', fechainicio);
        //     formData.append('fechafin', fechafin);

        //     // Agrega el ID de receta a los datos antes de enviar
        //     formData.append('planificacion_id', '{{ $idPlanificacion->id }}');

        //     axios.post('{{ route("guardar.planificacion") }}', formData)
        //         .then(function(response) {
        //             console.log(response.data);
        //             Swal.fire({
        //                 icon: 'success',
        //                 title: 'Éxito',
        //                 text: 'Datos guardados con éxito',
        //             }).then(function() {
        //             location.reload();
        //         });

        //         })
        //         .catch(function(error) {
        //             console.error(error);
        //             Swal.fire({
        //                 icon: 'error',
        //                 title: 'Error',
        //                 text: 'Hubo un problema al guardar los datos',
        //             });
        //         });
        // }

        // function actualizarSeccion() {
        //     console.log('Se está llamando a actualizarSeccion');
        //     $('#tablaplanificacionactivos').load(location.href + ' #tablaplanificacionactivos', function() {
        //         console.log('Sección actualizada con éxito');
        //         // 

        //     });

        // }
    </script>

    @include('sweetalert::alert')
</x-app-layout>