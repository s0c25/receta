<div>
    <!-- Tooltip Trigger Button -->
    <div x-data="{ open: false }">
        <button  @click="open = !open" class="bg-red-400 hover:bg-red-500 text-white font-bold py-2 px-4 rounded-md hover:py-3 hover:px-6 border-red-400  hover:border-red-400 ">
            Enviar Sugerencia
        </button>

        <!-- Tooltip Content -->
        <div   x-show="open" @click.away="open = false" class="tooltip-content p-4 bg-red-400 border rounded shadow-md md:w-3/12 -translate-y-full  ">
        <form  method="POST" action="{{ route('cliente.send') }}">
        @csrf

                <div class="mb-4">
                    <label for="message" class="block text-gray-700 text-sm font-bold mb-2">Sugerencia:</label>
                    <textarea maxlength="100" required id="message" name="message" rows="3" class="border rounded w-full py-2 px-3" placeholder="Ingresa Sugerencia"></textarea>
                </div>

                <button type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded">Enviar</button>
            </form>
        </div>
    </div>
</div>