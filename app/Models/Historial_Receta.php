<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial_Receta extends Model
{
    use HasFactory;
    

    protected $table = 'historial__recetas';

    protected $fillable = [
        'user_id',
        'receta_id',
        // Otros campos que deseas permitir en asignación masiva
    ];

   
}
