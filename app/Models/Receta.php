<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    use HasFactory;
    protected $table = 'recetas';

    protected $fillable = [
        'foto_receta',
        'nombre_receta',
        'tiempo_preparacion',
        'porciones',
        'calorias',
        'categoria_id',
        'slug',
        'preparacion',
        'estatus',
    ];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }

    public function ingredientes()
    {
        return $this->hasMany(Receta_Ingrediente::class, 'id_receta');
    }
    public function receta()
    {
        return $this->belongsTo(Receta::class, 'receta_id');
    }
}
