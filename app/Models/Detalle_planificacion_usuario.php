<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detalle_planificacion_usuario extends Model
{
    use HasFactory;

    protected $table = 'detalle_planificacion_usuarios';

    protected $fillable = [
        'planificacion_id',
        'receta_id',
        'hora_comida',
       'estatus',
       'dia_id',
       'fechafin',
       'fechainicio',
       'user_id'
       
    ];
}
