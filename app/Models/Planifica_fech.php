<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Planifica_fech extends Model
{
    use HasFactory;
    protected $table = 'planifica_feches';

    protected $fillable = [
        'user_id',
        'planificacion_id',
        'fechafin',
        'fechainicio',
    ];

    
}
