<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comunicacion extends Model
{
    use HasFactory;

    protected $table = 'comunicacions';

    protected $fillable = [
        'user_id',
        'mensaje',
        'estatus',
        'mensaje_admin',
    ];
}
