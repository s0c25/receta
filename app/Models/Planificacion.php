<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Planificacion extends Model
{
    use HasFactory;

    protected $table = 'planificacions';

    protected $fillable = [
        'nombre',
        'slug',
        'estatus',
        'foto_planificacion',
        'fechafin',
        'fechainicio'
       
    ];
}
