<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detalle_planificacion extends Model
{
    use HasFactory;

    protected $table = 'detalle_planificacions';

    protected $fillable = [
        'planificacion_id',
        'receta_id',
        'hora_comida',
       'estatus',
       'dia_id',
       'fechafin',
       'fechainicio',
    ];
}
