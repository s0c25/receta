<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    use HasFactory;

    protected $table = 'favoritos';

    protected $fillable = [
        'receta_id',
        'user_id',
        'estatus',
        // Otros campos que deseas permitir en asignación masiva
    ];

    public function receta()
    {
        return $this->belongsTo(Receta::class, 'receta_id');
    }
}
