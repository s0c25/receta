<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pibot_planificaciones extends Model
{
    use HasFactory;
    protected $table = 'pibot_planificaciones';

    protected $fillable = [
        'planificacion_id',
        'user_id'
        // Otros campos que deseas permitir en asignación masiva
    ];

    

}
