<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receta_Ingrediente extends Model
{
    use HasFactory;

    protected $table = 'receta__ingredientes';

    protected $fillable = [
        'id_receta',
        'id_ingrediente',
        'id_unidad',
        'cantidad',
        'estatus'
    ];

    public function ingrediente()
    {
        return $this->belongsTo(Ingrediente::class, 'id_ingrediente');
    }

    public function medida()
    {
        return $this->belongsTo(Medida::class, 'id_unidad');
    }
}
