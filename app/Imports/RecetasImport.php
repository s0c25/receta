<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Models\Edad;
use Illuminate\Support\Str;


class RecetasImport implements ToCollection, WithHeadingRow, WithValidation
{
    public $data;

    public function collection(Collection $rows)
    {
        $this->data = $rows;
    }

    public function rules(): array
    {

        return [
            'nombre_receta' => 'required|string|max:255',
            'foto_receta' => 'required', // max:2048 es el tamaño máximo en kilobytes (2MB en este caso)
            'preparacion' => 'required|string',
            'tiempo_preparacion' => 'required|string|max:255',
            'porciones' => 'required|string|max:255',
            'calorias' => 'required|string|max:255',
            'estatus' => 'required',
            'cantidad' => 'required',
            'nombre_ingrediente' => 'required',
            'nombre_unidad' => 'required',
            'categoria_receta' => 'required',
        ];
    }
}
    