<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;

class UploadRecetas extends Component
{

    use WithFileUploads;
    public $nombre_receta,$foto_receta,
    $preparacion,$tiempo_preparacion,
    $porciones,$calorias,$categoria;
    public $open = false;
    public function render()
    {
        return view('livewire.upload-recetas');
    }

    public function cerrar()
    {
        $this->reset();
        $this->open = false;
    }
}
