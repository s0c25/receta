<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Comunicacion;
use App\Models\Receta_Ingrediente;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;


class Mensajes extends Component
{
    use WithPagination;
    use LivewireAlert;

    public function render()
    {
        $com = Comunicacion::where('estatus','activo')
            ->leftjoin('users','users.id','=','comunicacions.user_id')
            ->select('users.name','users.profile_photo_path',
            'comunicacions.mensaje','comunicacions.id','comunicacions.mensaje_admin')
            ->paginate(3);
            
        $cliente = Comunicacion::where('estatus','respondido')
            ->leftjoin('users','users.id','=','comunicacions.user_id')
            ->select('users.name','users.profile_photo_path',
            'comunicacions.mensaje','comunicacions.id','comunicacions.mensaje_admin')
            ->paginate(3);
        return view('livewire.mensajes', compact('com','cliente'));
    }
}
