<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Receta;
use App\Models\User;
use App\Models\Receta_Ingrediente;
use App\Models\Historial_Receta;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Illuminate\Support\Facades\Auth;


class Historialver extends Component
{

    use WithPagination;
    use LivewireAlert;

    protected $paginationTheme = 'tailwind';
    public $nombre_receta, $foto_receta,
        $preparacion, $tiempo_preparacion,
        $porciones, $count, $fields;

    public $searchTerm;
    public $orderType = 'asc';

    public function toggleOrderType()
    {
        $this->orderType = ($this->orderType == 'asc') ? 'desc' : 'asc';
    }

    public function render()
    {

        $userId = Auth::user();

        $query = Historial_Receta::where('historial__recetas.estatus', 'activo')
            ->where('historial__recetas.user_id', $userId->id)
            ->leftJoin('recetas', 'recetas.id', '=', 'historial__recetas.receta_id')
            ->leftJoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
            ->leftJoin('users', 'users.id', '=', 'historial__recetas.user_id')
            ->select(
                'recetas.nombre_receta',
                'recetas.porciones',
                'recetas.foto_receta',
                'categorias.nombre_categoria',
                'recetas.id as idreceta',
                'historial__recetas.id as idFavorito',

            );

        if ($this->searchTerm) {
            $query->where(function ($subquery) {
                $subquery->where('recetas.nombre_receta', 'like', "%$this->searchTerm%")
                    ->orWhere('categorias.nombre_categoria', 'like', "%$this->searchTerm%");
            });
        }

        $historial_receta = $query->paginate(5);

        return view('livewire.historialver', [
            'historiales' => $historial_receta,
        ]);
    }


    public function search()
    {
        $this->resetPage(); // Reinicia el número de página al realizar una búsqueda
    }

}
