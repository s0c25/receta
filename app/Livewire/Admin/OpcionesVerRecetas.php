<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Receta_Ingrediente;

class OpcionesVerRecetas extends Component
{
    public $idreceta, $datos,$fields;

    public function render()
    {
        $arawato = Receta_Ingrediente::where('estatus', 'activo')
        ->where('id_receta', $this->idreceta)
        ->leftJoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
        ->leftJoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
        ->select(
            'medidas.nombre_unidad as nombre_unidad',
            "ingredientes.nombre_ingrediente as nombre_ingrediente",
        )
        ->get();
    
    $this->datos = $arawato;
    
    return view('livewire.admin.opciones-ver-recetas');
    
    }

    public function recibo($idreceta)
    {
        $this->idreceta = $idreceta;
    }
}
