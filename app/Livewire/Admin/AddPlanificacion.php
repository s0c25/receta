<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Planificacion;
use App\Models\Receta;
use Livewire\WithPagination;

class AddPlanificacion extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $open = false,$nombre_planificacion,$recetas;

    public function render()
    {
        return view('livewire.admin.add-planificacion');
    }
    public function cerrar()
    {
        $this->open = false;

    }

   
}
