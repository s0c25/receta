<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Categoria;

class AddReceta extends Component
{
    use WithFileUploads;

    public $open = false;
    public $nombre_receta,$foto_receta,
    $preparacion,$tiempo_preparacion,
    $porciones,$calorias,$categoria,$fields;

    public function render()
    {
        return view('livewire.admin.add-receta');
    }
    public function mount()
    {
        $this->categoria = Categoria::get();
    }
    public function cerrar()
    {
        $this->open = false;
        $this->reset();

    }
}
