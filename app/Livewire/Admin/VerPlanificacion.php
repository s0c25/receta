<?php

namespace App\Livewire\Admin;

use Livewire\Component;

class VerPlanificacion extends Component
{
    public function render()
    {
        return view('livewire.admin.ver-planificacion');
    }
}
