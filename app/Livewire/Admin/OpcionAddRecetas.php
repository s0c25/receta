<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Ingrediente;
use App\Models\Medida;

class OpcionAddRecetas extends Component
{
    public $ingredientes,$unidadesMedida,
    $ingredientesSeleccionados,$medidas,$fields;
    public $campos = [];

    public function agregarCampo()
    {
        $this->campos[] = '';
    }

    public function eliminarCampo($index)
    {
        unset($this->campos[$index]);
    }
    

    public function render()
    {
        return view('livewire.admin.opcion-add-recetas');
    }

    public function mount()
    {
        $this->ingredientes = Ingrediente::get();
        $this->medidas = Medida::get();

    }

}
