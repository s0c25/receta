<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Planificacion;
use App\Models\Detalle_planificacion;
use App\Models\Detalle_planificacion_usuario;
use App\Models\Receta_Ingrediente;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Illuminate\Support\Facades\Auth;

class ShowPlanificacion extends Component
{

    use WithPagination;
    use LivewireAlert;

    protected $paginationTheme = 'tailwind';
    public $count;

    public $searchTerm;
    public $orderType = 'asc';

    public function toggleOrderType()
    {
        $this->orderType = ($this->orderType == 'asc') ? 'desc' : 'asc';
    }


    public function render()
    {

        $user = Auth::user();
        if ($user->hasRole('admin')) {

            $query = Planificacion::where('estatus','activo')->orwhere('estatus','por-activar')->paginate(5);

            if ($this->searchTerm) {
                $query->where(function ($subquery) {
                    $subquery->where('nombre', 'like', "%$this->searchTerm%");
                });
            } else {
                return view('livewire.admin.show-planificacion', [
                    'planificacion' => Planificacion::where('estatus','activo')->orwhere('estatus','por-activar')->orderBy('nombre', $this->orderType)->paginate(5),
                ]);
            }
    
    
    
            return view('livewire.admin.show-planificacion', [
                'planificacion' => Planificacion::where('estatus','activo')
                ->orwhere('estatus','por-activar')->paginate(5),
            ]);
        }else{
            
            $query = Planificacion::where('estatus','activo')->paginate(5);

            if ($this->searchTerm) {
                $query->where(function ($subquery) {
                    $subquery->where('nombre', 'like', "%$this->searchTerm%");
                });
            } else {
                return view('livewire.admin.show-planificacion', [
                    'planificacion' => Planificacion::where('estatus','activo')->orderBy('nombre', $this->orderType)->paginate(5),
                ]);
            }
    
    
    
            return view('livewire.admin.show-planificacion', [
                'planificacion' => Planificacion::where('estatus','activo')->paginate(5),
            ]);
        } 
        

    }

    public function delete($id)
    {
        Planificacion::where('id',$id)->update(['estatus'=>'inactivo']);
        Detalle_planificacion::where('planificacion_id',$id)->update(['estatus'=>'inactivo']);
        // Detalle_planificacion_usuario::where('planificacion_id',$id)->update(['estatus'=>'inactivo']);

        $this->alert('info', 'Planificacion desactivada', [
            'position' => 'center',
            'timer' => 5000,
            'toast' => true,
            'confirmButtonText' => 'Ok',
            'text' => 'Desactivado',
            'timerProgressBar' => true,
           ]);
           $this->reset();

    }

    public function clean()
    {
        $this->reset();
        $this->mount();
    }

    public function ver($id)
    {
        dd($id);
    }

    public function search()
    {
        $this->resetPage(); // Reinicia el número de página al realizar una búsqueda
    }

    public function mount()
    {
        $this->count = Planificacion::where('estatus','activo')->get()->count();
    }
}
