<?php

namespace App\Livewire\Admin;

use App\Models\Receta_Ingrediente;
use Livewire\Component;
use App\Models\Receta;

class VerReceta extends Component
{

    public $open = false;
    public $row,$qwe;
    public $nombre_receta,$foto_receta,
    $preparacion,$tiempo_preparacion,
    $porciones,$calorias,$categoria,$idreceta,$fields;


    public function render()
    {
        return view('livewire.admin.ver-receta');
    }
    public function mount($row)
    {
        $data = Receta::find($row->id);
        $this->nombre_receta = $data->nombre_receta;
        $this->categoria = $data->categoria->nombre_categoria;
        $this->foto_receta =$data->foto_receta;
        $this->tiempo_preparacion = $data->tiempo_preparacion;
        $this->preparacion = $data->preparacion;
        $this->porciones = $data->porciones;
        $this->calorias = $data->calorias;
        $this->idreceta = $data->id;
        
    }
}
