<?php

namespace App\Livewire\Admin;

use Livewire\Component;
use App\Models\Receta;
use App\Models\Receta_Ingrediente;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class ShowRecetas extends Component
{
    use WithPagination;
    use LivewireAlert;

    protected $paginationTheme = 'tailwind';
    public $nombre_receta, $foto_receta,
        $preparacion, $tiempo_preparacion,
        $porciones, $count,$fields;

    public $searchTerm;
    public $orderType = 'asc';

    public function toggleOrderType()
    {
        $this->orderType = ($this->orderType == 'asc') ? 'desc' : 'asc';
    }

    public function render()
    {
        $query = Receta::with('categoria')->where('estatus', 'activo');

        if ($this->searchTerm) {
            $query->where(function ($subquery) {
                $subquery->where('nombre_receta', 'like', "%$this->searchTerm%")
                    ->orWhereHas('categoria', function ($subquery) {
                        $subquery->where('nombre_categoria', 'like', "%$this->searchTerm%");
                    });
            });
        } else {
            return view('livewire.admin.show-recetas', [
                'recetas' => Receta::where('estatus','activo')->orderBy('nombre_receta', $this->orderType)->paginate(10),
            ]);
        }


        $recetas = $query->paginate(10);

        return view('livewire.admin.show-recetas', [
            'recetas' => $recetas,
        ]);
    }

    public function delete($id)
    {
        Receta::where('id',$id)->update(['estatus'=>'inactivo']);
        Receta_Ingrediente::where('id_receta',$id)->update(['estatus'=>'inactivo']);
        
        $this->alert('info', 'Receta desactivada', [
            'position' => 'center',
            'timer' => 5000,
            'toast' => true,
            'confirmButtonText' => 'Ok',
            'text' => 'Recuerda que las recetas no se borran, solo se deshabilitan',
            'timerProgressBar' => true,
           ]);
           $this->reset();

    }

    public function clean()
    {
        $this->reset();
        $this->mount();
    }

    public function ver($id)
    {
        dd($id);
    }

    public function search()
    {
        $this->resetPage(); // Reinicia el número de página al realizar una búsqueda
    }

    public function mount()
    {
        $this->count = Receta::get()->where('estatus', 'activo')->count();
    }
}
