<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Ingrediente;
use App\Models\Medida;
use Illuminate\Support\Str;

class ItemsNuevos extends Component
{
    public $open = false,$fields;
    public $state = [
        'medida' => '',
        'ingrediente' => '',
    ];
    public function render()
    {
        return view('livewire.items-nuevos');
    }

    public function cerrar()
    {
        $this->reset();
        $this->open = false;
    }

    public function save_ingrediente()
    {
        $this->validate([
            'state.ingrediente' => 'required|', // Ajusta la validación según tus necesidades
        ]);

        // Guarda en la base de datos o realiza la lógica que necesites
        Ingrediente::create([
            'nombre_ingrediente' => $this->state['ingrediente'],
            'slug' => Str::slug($this->state['ingrediente']),
        ]);

        // Limpia el campo después de guardar
        $this->dispatch('saved');

        // $this->dispatch('refresh-navigation-menu');

        $this->state['ingrediente'] = '';
    }

    public function save_medida()
    {
        $this->validate([
            'state.medida' => 'required|', // Ajusta la validación según tus necesidades
        ]);

        // Guarda en la base de datos o realiza la lógica que necesites
        Medida::create([
            'nombre_unidad' => $this->state['medida'],
            'slug' => Str::slug($this->state['medida']),

        ]);

        $this->dispatch('savedd');

        // Limpia el campo después de guardar
        $this->state['medida'] = '';
    }
}
