<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Receta;
use App\Models\User;
use App\Models\Receta_Ingrediente;
use App\Models\Favorito;
use App\Models\Planificacion;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Illuminate\Support\Facades\Auth;


class Favoritosver extends Component
{

    use WithPagination;
    use LivewireAlert;

    protected $paginationTheme = 'tailwind';
    public $nombre_receta, $foto_receta,
        $preparacion, $tiempo_preparacion,
        $porciones, $count, $fields;

    public $searchTerm;
    public $orderType = 'asc';

    public function planificacion()
    {
        dd('dsadas');
    }

    public function toggleOrderType()
    {
        $this->orderType = ($this->orderType == 'asc') ? 'desc' : 'asc';
    }

    public function render()
    {

        $userId = Auth::user();

        $query = Favorito::where('favoritos.estatus', 'activo')
            ->where('favoritos.user_id', $userId->id)
            ->leftJoin('recetas', 'recetas.id', '=', 'favoritos.receta_id')
            ->leftJoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
            ->leftJoin('users', 'users.id', '=', 'favoritos.user_id')
            ->select(
                'recetas.nombre_receta',
                'recetas.porciones',
                'recetas.foto_receta',
                'categorias.nombre_categoria',
                'recetas.id as idreceta',
                'favoritos.id as idFavorito',

            );

        if ($this->searchTerm) {
            $query->where(function ($subquery) {
                $subquery->where('recetas.nombre_receta', 'like', "%$this->searchTerm%")
                    ->orWhere('categorias.nombre_categoria', 'like', "%$this->searchTerm%");
            });
        }

        $favoritos = $query->paginate(5);

        return view('livewire.favoritosver', [
            'favoritos' => $favoritos,
        ]);
    }


    public function delete($id)
    {
        Favorito::where('id', $id)->delete();

        $this->alert('info', 'Eliminada de Favoritos', [
            'position' => 'center',
            'timer' => 5000,
            'toast' => true,
            'confirmButtonText' => 'Ok',
            'text' => 'Recuerda que las recetas no se borran, solo se deshabilitan',
            'timerProgressBar' => true,
        ]);
        $this->reset();
    }

    public function ver($id)
    {
    }

    public function clean()
    {
        $this->reset();
        $this->mount();
    }



    public function search()
    {
        $this->resetPage(); // Reinicia el número de página al realizar una búsqueda
    }

    public function mount()
    {
        $userId = Auth::user();

        $this->count = Favorito::where('user_id', $userId->id)->where('estatus', 'activo')->get()->count();
    }
}
