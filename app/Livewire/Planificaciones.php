<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Receta;
use App\Models\Detalle_planificacion_usuario;
use App\Models\pibot_planificaciones;
use App\Models\Planifica_fech;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Illuminate\Support\Facades\Auth;


class Planificaciones extends Component
{
    use WithPagination;
    use LivewireAlert;

    protected $paginationTheme = 'tailwind';
    public $open=false;

    public $searchTerm;
    public $orderType = 'asc';


    public function render()
    {
        $user = Auth::user();

        $datas =  pibot_planificaciones::where('pibot_planificaciones.user_id', $user->id)
        ->leftJoin('planificacions', 'planificacions.id', '=', 'pibot_planificaciones.planificacion_id')
        ->select('planificacions.nombre as nombreplanificacions',
                'planificacions.id as planificacionsid')
        ->get();
        // dd($datas);
        // dd($datas);
        return view('livewire.planificaciones', compact('datas'));
    }

    public function cerrar()
    {
        $this->open = false;
        $this->reset();
    }

    public function delete($id)
    {
        pibot_planificaciones::where('planificacion_id',$id)->delete();
        Planifica_fech::where('planificacion_id',$id)->delete();
        // Detalle_planificacion_usuario::where('planificacion_id',$id)->update(['estatus'=>'inactivo']);

        $this->alert('info', 'Planificacion desactivada', [
            'position' => 'center',
            'timer' => 5000,
            'toast' => true,
            'confirmButtonText' => 'Ok',
            'text' => 'Desactivado',
            'timerProgressBar' => true,
           ]);
           $this->reset();

    }
}
