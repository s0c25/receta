<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Categoria;
use App\Models\Ingrediente;
use App\Models\Medida;

class AddIngredienteOtro extends Component
{
    public $ingredientes,$unidadesMedida,
    $ingredientesSeleccionados,$medidas,$fields;
    public $campos = [];

    public function agregarCampo()
    {
        $this->campos[] = '';
    }

    public function eliminarCampo($index)
    {
        unset($this->campos[$index]);
    }
    
    public function render()
    {
        return view('livewire.add-ingrediente-otro');
    }

    public function mount()
    {
        $this->ingredientes = Ingrediente::get();
        $this->medidas = Medida::get();
        $this->reset();
    }
}
