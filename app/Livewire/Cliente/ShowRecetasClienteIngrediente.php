<?php

namespace App\Livewire\Cliente;

use App\Models\Ingrediente;
use Livewire\Component;
use App\Models\Receta;
use App\Models\Receta_Ingrediente;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
class ShowRecetasClienteIngrediente extends Component
{
    public $open = false;
    public $selectIngrediente = [], $datas=[];
    public $selectcalorias;
    public $selectcoccion;

    public function render()
    {
        $ingredientes = Ingrediente::get();

        $calorias = Receta::select('calorias', 'tiempo_preparacion', 'id')->get();

        // Realizar la consulta utilizando Eloquent
        $recetas = Receta::whereHas('ingredientes', function ($query) {
            $query->where('id', $this->selectIngrediente);
        })
            ->select('nombre_receta', 'porciones', 'id', 'foto_receta')
            ->paginate(5);  // Ajusta la paginación según tus necesidades


        return view('livewire.cliente.show-recetas-cliente-ingrediente', [
            'recetas' => $recetas,
            'ingredientes' => $ingredientes,
            'calorias' => $calorias,
        ]);
    }


    public function realizarBusqueda()
    {

        $recetasIds = [];

        // Condiciones de calorias y tiempo de preparación
        $hol = DB::table('recetas')
        ->where(function ($query) {
            if ($this->selectcalorias == "- 500") {
                $query->where('calorias', '<=', 500);
            } elseif ($this->selectcalorias == "+ 500") {
                $query->where('calorias', '>=', 500);
            }
        })
        ->where(function ($query) {
            if ($this->selectcoccion == "Mayor 20") {
                $query->where('tiempo_preparacion', '>=', 20);
            } elseif ($this->selectcoccion == "Menor 20") {
                $query->where('tiempo_preparacion', '<=', 20);
            }
        })
        ->pluck('id')
        ->toArray();
    
    $recetasIds = [];
    
    foreach ($this->selectIngrediente as $ingredienteId) {
        $recetasIds[] = DB::table('receta__ingredientes')
            ->where('receta__ingredientes.estatus', 'activo')
            ->where('receta__ingredientes.id_ingrediente', $ingredienteId)
            ->whereIn('receta__ingredientes.id_receta', $hol) // Aplicar condiciones de calorias y tiempo
            ->pluck('receta__ingredientes.id_receta')
            ->toArray();
    }

        // Intersección de los arrays para obtener las recetas que tienen todos los ingredientes seleccionados
        $recetasComunes = count($recetasIds) > 0 ? array_intersect(...$recetasIds) : [];

        // Obtener detalles de las recetas
        $resultadosAgrupados = Receta::whereIn('id', $recetasComunes)
            ->select('nombre_receta', 'porciones', 'id', 'calorias', 'tiempo_preparacion')
            ->get()
            ->groupBy('id');

            $datosRecetas = [];

            foreach ($resultadosAgrupados as $key => $row) {
            
                foreach ($row as $modelo) {
                    $datosReceta = [
                        'nombre_receta' => $modelo['nombre_receta'],
                        'porciones' => $modelo['porciones'],
                        'calorias' => $modelo['calorias'],
                        'tiempo_preparacion' => $modelo['tiempo_preparacion'],
                        'id' => $modelo['id'],
                    ];
                
                    // Agregar el array de datos de la receta al array principal
                    $datosRecetas[] = $datosReceta;

                }
            }
            
        $this->datas = $datosRecetas;
    }

    public function cerrar()
    {
        $this->open = false;
        $this->reset();
    }
}
