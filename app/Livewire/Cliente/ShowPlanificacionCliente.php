<?php

namespace App\Livewire\Cliente;

use Livewire\Component;

class ShowPlanificacionCliente extends Component
{
    public function render()
    {
        return view('livewire.cliente.show-planificacion-cliente');
    }
}
