<?php

namespace App\Livewire\Cliente;

use App\Models\Ingrediente;
use Livewire\Component;
use App\Models\Receta;
use App\Models\Receta_Ingrediente;
use Livewire\WithPagination;
use Jantinnerezo\LivewireAlert\LivewireAlert;


class ShowRecetasCliente extends Component
{

    use WithPagination;
    use LivewireAlert;

    protected $paginationTheme = 'tailwind';
    public $nombre_receta, $foto_receta,
        $preparacion, $tiempo_preparacion,
        $porciones, $count, $fields;

    public $searchTerm, $selectIngrediente;
    public $orderType = 'asc';
    public $resultados = [];

    public function toggleOrderType()
    {
        $this->orderType = ($this->orderType == 'asc') ? 'desc' : 'asc';
    }

    public function render()
    {
        $ingredientes = Ingrediente::get();

        $query = Receta::with('categoria')->where('estatus', 'activo');


        if ($this->searchTerm) {
            $query->where(function ($subquery) {
                $subquery->where('nombre_receta', 'like', "%$this->searchTerm%")
                    ->orWhereHas('categoria', function ($subquery) {
                        $subquery->where('nombre_categoria', 'like', "%$this->searchTerm%");
                    });
            });
        } else {
            return view('livewire.cliente.show-recetas-cliente', [
                'recetas' => Receta::where('estatus', 'activo')->orderBy('nombre_receta', $this->orderType)->paginate(10),
                'ingredientes' => $ingredientes,
            ]);
        }


        $recetas = $query->paginate(10);


        return view('livewire.cliente.show-recetas-cliente', [
            'recetas' => $recetas,
            'ingredientes' => $ingredientes,

        ]);
    }


    public function clean()
    {
        $this->reset();
    }


    public function search()
    {
        $this->resetPage(); // Reinicia el número de página al realizar una búsqueda
    }

    public function buscaIn()
    {
        if ($this->selectIngrediente){

            $this->reset();
            $ingredientes = Ingrediente::get();


              // Realizar la consulta utilizando Eloquent
                $recetas = Receta::whereHas('ingredientes', function ($query) {
                    $query->where('id', $this->selectIngrediente);
                })
                ->select('nombre_receta', 'porciones', 'id', 'foto_receta')
                ->paginate(1);  // Ajusta la paginación según tus necesidades


                return view('livewire.cliente.show-recetas-cliente', [
                    'recetas' => $recetas,
                    'ingredientes' => $ingredientes,
                ]);

        }
        
    }
}
