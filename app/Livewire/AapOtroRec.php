<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\Categoria;
use App\Models\Ingrediente;
use App\Models\Medida;

class AapOtroRec extends Component
{

    public $open = false;
    public $nombre_receta,$foto_receta,
    $preparacion,$tiempo_preparacion,
    $porciones,$calorias,$categoria,$fields;

    public $campos = [];

    public function agregarCampo()
    {
        $this->campos[] = '';
    }

    public function eliminarCampo($index)
    {
        unset($this->campos[$index]);
    }
    
    
    public function render()
    {
        return view('livewire.aap-otro-rec');
    }
    public function mount()
    {
        $this->categoria = Categoria::get();
    }
    public function cerrar()
    {
        $this->open = false;
        $this->reset();

    }
}
