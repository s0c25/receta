<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Models\Ingrediente;
use App\Models\Medida;
use App\Models\Receta;
use App\Models\User;
use App\Models\Receta_Ingrediente;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Favorito;
use App\Models\Historial_Receta;

use function PHPUnit\Framework\isNull;

class RecetasController extends Controller
{

   
    public function favorito($row)
    {
        $user = Auth::user();

        $favorito = Favorito::where('receta_id', $row)->get();
        if ($favorito != '[]') {
            $adminUser = User::role('admin')->first();
            // dd($adminUser);
            $idReceta = Receta::find($row);
            // dd($idReceta->id);
            if (!$idReceta) {
                alert()->success('Error', 'Error al ejecutar consulta de receta');
            }
            $data = Receta_Ingrediente::where('id_receta', $row)
                ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                ->select(
                    'ingredientes.nombre_ingrediente as nombre_ingrediente',
                    'medidas.nombre_unidad as nombre_unidad',
                    'receta__ingredientes.cantidad as cantidad',
                )
                ->get();
            alert()->success('Ya existe', 'Receta activa en favoritos');
            return view('cliente.recetas-view', compact('idReceta', 'data', 'adminUser'));
        } else {

            $favorito = new Favorito;
            $favorito->receta_id = $row;
            $favorito->user_id = $user->id;
            $favorito->estatus = 'activo';
            $favorito->save();

            $adminUser = User::role('admin')->first();
            // dd($adminUser);
            $idReceta = Receta::find($row);
            // dd($idReceta->id);
            if (!$idReceta) {
                alert()->success('Error', 'Error al ejecutar consulta de receta');
            }
            $data = Receta_Ingrediente::where('id_receta', $row)
                ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                ->select(
                    'ingredientes.nombre_ingrediente as nombre_ingrediente',
                    'medidas.nombre_unidad as nombre_unidad',
                    'receta__ingredientes.cantidad as cantidad',
                )
                ->get();
            alert()->success('Registro exitoso', 'Guardada en Favorito');

            return view('cliente.recetas-view', compact('idReceta', 'data', 'adminUser'));
        }
    }
    public function index()
    {

        $user = Auth::user();
        if ($user->hasRole('admin')) {
            return view('admin.recetas');
        }

        if ($user->hasRole('cliente')) {
            $adminUser = User::role('admin')->first();
            // dd($adminUser);
            $idReceta = Receta::first();

            // dd($idReceta);
            if ($idReceta == null) {
                alert()->error('Error', 'No tenemos recetas');


                return redirect()->route('dashboard-cliente');
            }else{
            $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
                ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                ->select(
                    'ingredientes.nombre_ingrediente as nombre_ingrediente',
                    'medidas.nombre_unidad as nombre_unidad',
                    'receta__ingredientes.cantidad as cantidad',
                )
                ->get();
            return view('cliente.recetas-view', compact('idReceta', 'data', 'adminUser'));
        }
        }
        return "No tienes permisos suficientes.";
    }
    public function guardarDatos(Request $request)
    {

        // Valida los datos recibidos del formulario
        $request->validate([
            'cantidad' => 'required|',
            'idReceta' => 'required|',
            'ingredientes' => 'required|',
            'medidas' => 'required|',
        ]);


        // Obtiene los datos del formulario
        $cantidades = $request->input('cantidad');
        $idReceta = $request->input('idReceta');
        $ingredientes = $request->input('ingredientes');
        $medidas = $request->input('medidas');


        // Procesa y guarda los datos en la base de datos
        foreach ($cantidades as $key => $cantidad) {
            Receta_Ingrediente::create([
                'cantidad' => $cantidad,
                'id_receta' => $idReceta,
                'id_ingrediente' => $ingredientes[$key],
                'id_unidad' => $medidas[$key],
            ]);
        }

        return response()->json(['mensaje' => 'Datos guardados con éxito']);
    }

    public function eliminarDatos($id)
    {

        $ingrediente = Receta_Ingrediente::findOrFail($id);

        // Elimina el ingrediente de la base de datos
        $ingrediente->delete();

        return response()->json(['mensaje' => 'Ingrediente eliminado con éxito']);
    }

    public function save_receta(Request $request)
    {

        if ($request->foto_receta != '{}') {
            $validator = Validator::make($request->all(), [
                'foto_receta' => 'required|file|mimes:png,jpg,jpeg',
                'nombre_receta' => 'required|string|max:255',
                'tiempo_preparacion' => 'required|string|max:255',
                'porciones' => 'required|string|max:255',
                'calorias' => 'required|string|max:255',
                'categoria' => 'required|string|max:255',
                'preparacion' => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => ['Los datos no cumples los requisitos(Foto Cambia)']]);
            }

            $fotoRecetaFile = $request->foto_receta;
            $fileName = $fotoRecetaFile->getClientOriginalName();
            $filePath = 'recetas/' . Str::slug($request->nombre_receta) . '/' . Str::slug($fileName);
            Storage::disk('public')->put($filePath, file_get_contents($fotoRecetaFile));
            $datosRecetas = [
                'foto_receta' => $filePath,
                'preparacion' => $request->preparacion,
                'tiempo_preparacion' => $request->tiempo_preparacion,
                'porciones' => $request->porciones,
                'calorias' => $request->calorias,
                'estatus' => 'activo',
                'categoria_id' => $request->categoria,
                'slug' => $request->slug,
            ];
            $idReceta = Receta::where('slug', $datosRecetas['slug'])
                ->first();
            if ($idReceta) {
                $idReceta->update($datosRecetas);
                // alert()->info('Receta existente', 'Receta actualizada');
                return response()->json(['success' => ['Receta actualizada']]);
            }
        } else {
            // return response()->json(['success' => $request->all()]);

            $validator = Validator::make($request->all(), [
                'tiempo_preparacion' => 'required|string|max:255',
                'porciones' => 'required|string|max:255',
                'calorias' => 'required|string|max:255',
                'categoria' => 'required|string|max:255',
                'preparacion' => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => ['Los datos no cumples los requisitos(Foto no cambia)']]);
            }

            $datosRecetas = [
                'preparacion' => $request->preparacion,
                'tiempo_preparacion' => $request->tiempo_preparacion,
                'porciones' => $request->porciones,
                'calorias' => $request->calorias,
                'estatus' => 'activo',
                'categoria_id' => $request->categoria,
                'slug' => $request->slug,
            ];
            $idReceta = Receta::where('slug', $datosRecetas['slug'])
                ->first();
            if ($idReceta) {
                $idReceta->update($datosRecetas);
                // alert()->info('Receta existente', 'Receta actualizada');
                return response()->json(['success' => ['Receta actualizada']]);
            }
        }
    }

    public function save(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'foto_receta' => 'required|file|mimes:png,jpg',
            'nombre_receta' => 'required|string|max:255',
            'tiempo_preparacion' => 'required|string|max:255',
            'porciones' => 'required|string|max:255',
            'calorias' => 'required|string|max:255',
            'categoria' => 'required|string|max:255',
            'preparacion' => 'required|string',
            'select1.*' => 'required',
            'select2.*' => 'required',
            'cantidad.*' => ['required'],
        ]);
        try {
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $fotoRecetaFile = $request->file('foto_receta');
            $fileName = $fotoRecetaFile->getClientOriginalName();
            $filePath = 'recetas/' . Str::slug($request->input('nombre_receta')) . '/' . Str::slug($fileName);
            Storage::disk('public')->put($filePath, file_get_contents($fotoRecetaFile));
            $datosRecetas = [
                'nombre_receta' => $request->input('nombre_receta'),
                'foto_receta' => $filePath,
                'preparacion' => $request->input('preparacion'),
                'tiempo_preparacion' => $request->input('tiempo_preparacion'),
                'porciones' => $request->input('porciones'),
                'calorias' => $request->input('calorias'),
                'estatus' => 'activo',
                'categoria_id' => $request->input('categoria'),
                'slug' => Str::slug($request->input('nombre_receta')),
            ];
            $idReceta = Receta::where('slug', $datosRecetas['slug'])
                ->first();
            if ($idReceta) {

                $idReceta->update($datosRecetas);
                alert()->info('Receta existente', 'La receta' . ' ' . $datosRecetas['nombre_receta'] . ' ' . 'fue actualizada' );
                return redirect()->route('admin.recetas');
            } else {
                $receta = new Receta();
                $receta->nombre_receta = $request->input('nombre_receta');
                $receta->preparacion = $request->input('preparacion');
                $receta->tiempo_preparacion = $request->input('tiempo_preparacion');
                $receta->porciones = $request->input('porciones');
                $receta->calorias = $request->input('calorias');
                $receta->categoria_id = $request->input('categoria');
                $receta->slug = Str::slug($request->input('nombre_receta'));
                $receta->foto_receta =  $filePath;;
                $receta->save();
                $idReceta = $receta->id;
                $ingredientes = $request->ingrediente;
                $medidas = $request->medida;
                $cantidades = $request->cantidad;
                foreach ($ingredientes as $index => $idIngrediente) {
                    $idMedida = $medidas[$index];
                    $cantidadValue = $cantidades[$index];
                    $receta = new Receta_Ingrediente();
                    $receta->cantidad = $cantidadValue;
                    $receta->id_unidad = $idMedida;
                    $receta->id_ingrediente = $idIngrediente;
                    $receta->id_receta = $idReceta;
                    $receta->save();
                }

                alert()->success('Receta registrada', 'La receta' . ' ' . $datosRecetas['nombre_receta'] . ' ' . 'con el nombre unico' . ' ' . $datosRecetas['slug'] . ' ' . 'fue registrada');
                return redirect()->route('admin.recetas');
            }
        } catch (QueryException $e) {
            if ($e->getCode() == 23000) {
                alert()->warning('Error de Registro', 'Ya existe una receta con ese nombre. Por favor, elige otro nombre.');
                return back()->withError('Ya existe una receta con ese nombre. Por favor, elige otro nombre.');
            }
        } catch (ValidationException $e) {
            $errors = $e->validator->errors()->messages();
            alert()->warning('Error de Registro', $errors);
            return redirect()->route('admin.recetas');
        }
    }

    public function view($row)
    {

        $user = Auth::user();

        if ($user->hasRole('admin')) {

            $idReceta = Receta::find($row);
            if (!$idReceta) {
                alert()->success('Error', 'Error al ejecutar consulta de receta');
            }
            $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
                ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                ->select(
                    'ingredientes.nombre_ingrediente as nombre_ingrediente',
                    'medidas.nombre_unidad as nombre_unidad',
                    'receta__ingredientes.cantidad as cantidad',
                )
                ->get();
            return view('admin.recetas-view', compact('idReceta', 'data'));
        }

        if ($user->hasRole('cliente')) {
            $adminUser = User::role('admin')->first();

            $idReceta = Receta::find($row);
            if (!$idReceta) {
                alert()->success('Error', 'Error al ejecutar consulta de receta');
            }
            $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
                ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                ->select(
                    'ingredientes.nombre_ingrediente as nombre_ingrediente',
                    'medidas.nombre_unidad as nombre_unidad',
                    'receta__ingredientes.cantidad as cantidad',
                )
                ->get();
            return view('cliente.recetas-view', compact('idReceta', 'data', 'adminUser'));
        }
    }

    public function edit($row)
    {
        $idReceta = Receta::find($row);
        // dd($idReceta);
        $categoria = Categoria::get();
        $ingredientes = Ingrediente::get();
        $medidas = Medida::get();

        if (!$idReceta) {
            alert()->success('Error', 'Error al ejecutar consulta de receta');
        }
        $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
            ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
            ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
            ->select(
                'ingredientes.nombre_ingrediente as nombre_ingrediente',
                'medidas.nombre_unidad as nombre_unidad',
                'receta__ingredientes.cantidad as cantidad',
                'receta__ingredientes.id as idrecetaingrediente',
            )
            ->get();
        return view('admin.recetas-edit', compact('idReceta', 'data', 'categoria', 'ingredientes', 'medidas'));
    }

    public function mostrarSiguientePagina($currentId)
    {

        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $nextId = Receta::where('estatus','activo')->where('id', '>', $currentId)->min('id');
            if ($nextId === null) {
                $nextId = Receta::min('id');
            }
            if ($nextId !== null) {
                $idReceta = Receta::find($nextId);
                if (!$idReceta) {
                    alert()->success('Error', 'Error al ejecutar consulta de receta');
                }
                $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
                    ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                    ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                    ->select(
                        'ingredientes.nombre_ingrediente as nombre_ingrediente',
                        'medidas.nombre_unidad as nombre_unidad',
                        'receta__ingredientes.cantidad as cantidad',
                    )
                    ->get();
                $recorrido_mas = $idReceta->id;
                return view('admin.recetas-view', compact('idReceta', 'data', 'recorrido_mas'));
            }
        }

        if ($user->hasRole('cliente')) {
            $adminUser = User::role('admin')->first();

            $nextId = Receta::where('estatus','activo')->where('id', '>', $currentId)->min('id');
            if ($nextId === null) {
                $nextId = Receta::min('id');
            }
            if ($nextId !== null) {
                $idReceta = Receta::find($nextId);
                if (!$idReceta) {
                    alert()->success('Error', 'Error al ejecutar consulta de receta');
                }
                $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
                    ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                    ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                    ->select(
                        'ingredientes.nombre_ingrediente as nombre_ingrediente',
                        'medidas.nombre_unidad as nombre_unidad',
                        'receta__ingredientes.cantidad as cantidad',
                    )
                    ->get();
                $recorrido_mas = $idReceta->id;


                $gistorial = Historial_Receta::where('user_id', $user->id)
                    ->where('receta_id', $idReceta->id)
                    ->first();

                if ($gistorial) {
                    return view('cliente.recetas-view', compact('idReceta', 'data', 'recorrido_mas', 'adminUser'));
                } else {
                    Historial_Receta::create([
                        'user_id' => $user->id,
                        'receta_id' => $idReceta->id,
                    ]);
                    return view('cliente.recetas-view', compact('idReceta', 'data', 'recorrido_mas', 'adminUser'));
                }
            }
        }
    }

    public function mostrarPaginaAnterior($currentId)
    {
        $user = Auth::user();

        if ($user->hasRole('cliente')) {
            $adminUser = User::role('admin')->first();

            $prevId = Receta::where('estatus','activo')->where('id', '<', $currentId)->max('id');
            if ($prevId === null) {
                $prevId = Receta::max('id');
            }
            $idReceta = Receta::find($prevId);
            if (!$idReceta) {
                alert()->success('Error', 'Error al ejecutar consulta de receta');
                return redirect()->back();
            }
            $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
                ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                ->select(
                    'ingredientes.nombre_ingrediente as nombre_ingrediente',
                    'medidas.nombre_unidad as nombre_unidad',
                    'receta__ingredientes.cantidad as cantidad',
                )
                ->get();
            $recorrido_menos = $idReceta->id;
            // return view('cliente.recetas-view', compact('idReceta', 'data', 'recorrido_menos', 'adminUser'));
            $gistorial = Historial_Receta::where('user_id', $user->id)
                ->where('receta_id', $idReceta->id)
                ->first();
            if ($gistorial) {
                return view('cliente.recetas-view', compact('idReceta', 'data', 'recorrido_menos', 'adminUser'));
            } else {

                Historial_Receta::create([
                    'user_id' => $user->id,
                    'receta_id' => $idReceta->id,
                ]);
                return view('cliente.recetas-view', compact('idReceta', 'data', 'recorrido_menos', 'adminUser'));
            }
        }

        if ($user->hasRole('admin')) {

            $prevId = Receta::where('estatus','activo')->where('id', '<', $currentId)->max('id');
            if ($prevId === null) {
                $prevId = Receta::max('id');
            }
            $idReceta = Receta::find($prevId);
            if (!$idReceta) {
                alert()->success('Error', 'Error al ejecutar consulta de receta');
                return redirect()->back();
            }
            $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
                ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
                ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
                ->select(
                    'ingredientes.nombre_ingrediente as nombre_ingrediente',
                    'medidas.nombre_unidad as nombre_unidad',
                    'receta__ingredientes.cantidad as cantidad',
                )
                ->get();
            $recorrido_menos = $idReceta->id;



            return view('admin.recetas-view', compact('idReceta', 'data', 'recorrido_menos'));
        }
    }
}
