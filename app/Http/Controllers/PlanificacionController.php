<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Planificacion;
use App\Models\Detalle_planificacion;
use App\Models\Detalle_planificacion_usuario;
use App\Models\Receta;
use App\Models\Dia;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;

class PlanificacionController extends Controller
{
    public function index()
    {
        return view('admin.planificacion');
    }

    public function editar_planificacion($id)
    {
        // dd($id);
        $user = Auth::user();
        $row = $id;
        if ($user->hasRole('admin')) {
            $idPlanificacion = Planificacion::find($row);
            $recetas = Receta::where('estatus','activo')
                ->leftjoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
                ->select(
                    'recetas.nombre_receta as nombre_receta',
                    'recetas.id as id',
                    'categorias.nombre_categoria as nombre_categoria'
                )
                ->get();

            if (!$idPlanificacion) {
                alert()->success('Error', 'Error al ejecutar consulta de planificacion');
            }
            $data = Detalle_planificacion::where('planificacion_id', $idPlanificacion->id)
                ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
                ->leftjoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
                ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
                ->select(
                    'recetas.nombre_receta as nombre_receta',
                    'detalle_planificacions.hora_comida as hora_comida',
                    'detalle_planificacions.id as id',
                    'dias.nombre as nombre',
                    'categorias.nombre_categoria as nombre_categoria'
                )
                ->get();
            $dia = Dia::get();
            return view('admin.edit-planificacion', compact('idPlanificacion', 'data', 'recetas','dia'));
        }

        if ($user->hasRole('cliente')) {
            // dd('hola');
            $idPlanificacion = Planificacion::find($row);
            $recetas = Detalle_planificacion::where('id', $idPlanificacion->id)
            ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
            ->leftjoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
            ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
            ->select(
                'recetas.nombre_receta as nombre_receta',
                'detalle_planificacions.hora_comida as hora_comida',
                'detalle_planificacions.id as id',
                'dias.nombre as nombre',
                'categorias.nombre_categoria as nombre_categoria'
            )
            ->get();

            if (!$idPlanificacion) {
                alert()->success('Error', 'Error al ejecutar consulta de planificacion');
            }
           
            $data = Detalle_planificacion_usuario::where('planificacion_id', $idPlanificacion->id)
                ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacion_usuarios.receta_id')
                ->leftjoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
                ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacion_usuarios.dia_id')
                ->select(
                    'recetas.nombre_receta as nombre_receta',
                    'detalle_planificacion_usuarios.hora_comida as hora_comida',
                    'detalle_planificacion_usuarios.id as id',
                    'dias.nombre as nombre',
                    'categorias.nombre_categoria as nombre_categoria'
                )
                ->get();

            $dia = Dia::get();
            return view('admin.edit-planificacion', compact('idPlanificacion', 'data', 'recetas','dia'));

        }

    }

    public function ver_planificacion($row)
    {
        $user = Auth::user();
        // dd($user);
        if ($user->hasRole('admin')) {
                $idPlanificacion = Planificacion::find($row);
                if (!$idPlanificacion) {
                    alert()->success('Error', 'Error al ejecutar consulta de receta');
                }
                $data = Detalle_planificacion::where('planificacion_id', $idPlanificacion->id)
                    ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
                    ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
                    ->select(
                        'recetas.nombre_receta as nombre_receta',
                        'recetas.calorias as calorias',
                        'dias.nombre as nombre',
                        'detalle_planificacions.hora_comida as hora_comida',
                        'detalle_planificacions.id as id',
                    )
                    ->get();
                $idReceta = $idPlanificacion;
                return view('admin.ver-planificacion', compact('idReceta', 'data'));
            }
      

        
    }


    public function mostrarSiguientePagina($currentId)
    {
        $nextId = Planificacion::where('estatus','activo')->where('id', '>', $currentId)->min('id');
        if ($nextId === null) {
            $nextId = Planificacion::min('id');
        }
        if ($nextId !== null) {
            $idReceta = Planificacion::find($nextId);
            if (!$idReceta) {
                alert()->success('Error', 'Error al ejecutar consulta de planificacion');
            }
            $data = Detalle_planificacion::where('planificacion_id', $idReceta->id)
        ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
        ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
        ->select(
            'recetas.nombre_receta as nombre_receta',
            'dias.nombre as nombre',
            'recetas.calorias as calorias',
            'detalle_planificacions.hora_comida as hora_comida',
            'detalle_planificacions.id as id',
        )
        ->get();
            $recorrido_mas = $idReceta->id;
            return view('admin.ver-planificacion', compact('idReceta', 'data', 'recorrido_mas'));
        }
    }

    public function mostrarPaginaAnterior($currentId)
    {
        $prevId = Planificacion::where('estatus','activo')->where('id', '<', $currentId)->max('id');
        if ($prevId === null) {
            $prevId = Planificacion::max('id');
        }
        $idReceta = Planificacion::find($prevId);
        if (!$idReceta) {
            alert()->success('Error', 'Error al ejecutar consulta de receta');
            return redirect()->back();
        }
        $data = Detalle_planificacion::where('planificacion_id', $idReceta->id)
        ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
        ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
        ->select(
            'recetas.nombre_receta as nombre_receta',
            'dias.nombre as nombre',
            'recetas.calorias as calorias',
            'detalle_planificacions.hora_comida as hora_comida',
            'detalle_planificacions.id as id',
        )
        ->get();
        $recorrido_menos = $idReceta->id;
        return view('admin.ver-planificacion', compact('idReceta', 'data', 'recorrido_menos'));
    }

    public function edit($row)
    {
        $idPlanificacion = Planificacion::find($row);

        $recetas = Receta::where('estatus','activo')
            ->leftjoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
            ->select(
                'recetas.nombre_receta as nombre_receta',
                'recetas.id as id',
                'categorias.nombre_categoria as nombre_categoria'
            )
            ->get();

        // dd($recetas->categoria->nombre_categoria);

        if (!$idPlanificacion) {
            alert()->success('Error', 'Error al ejecutar consulta de planificacion');
        }
        $data = Detalle_planificacion::where('planificacion_id', $idPlanificacion->id)
            ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
            ->leftjoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
            ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
            ->select(
                'recetas.nombre_receta as nombre_receta',
                'dias.nombre as nombre',
                'recetas.calorias as calorias',
                'detalle_planificacions.id as id',
                'detalle_planificacions.hora_comida as hora_comida',
                'detalle_planificacions.id as planificacion_id',
                'categorias.nombre_categoria as nombre_categoria'
            )
            ->get();
        $dia= Dia::get();
        return view('admin.edit-planificacion', compact('idPlanificacion', 'data', 'recetas','dia'));
    }

    public function guardarDatos(Request $request)
    {

        $user = Auth::user();
        if ($user->hasRole('admin')) {

            try {

                if ($request->foto_planificacion == 'null') {
    

                    $recetas = $request->input('recetas');
                    $hora_inicio_inputs = $request->input('hora_inicio');
                    $dias = $request->input('dias');
                    $idPlanificacion = $request->input('idPlanificacion');
                    $fechafin = $request->input('fechafin');
                    $fechainicio = $request->input('fechainicio');

                    if (filled($fechainicio) && filled($fechafin)) {

                        Planificacion::where('id',$idPlanificacion)
                            ->update([
                                'estatus'=>'activo',
                                'fechafin'=>$fechafin,
                                'fechainicio'=>$fechainicio,
                            ]);
                    }else{

                        $hola = Planificacion::where('id',$idPlanificacion)
                        ->update([
                            'estatus'=>'activo',
                        ]);


                        foreach ($hora_inicio_inputs as $key => $hora_inicio) {
    
                            $carbonHora = Carbon::parse($hora_inicio);
                            $horaFormateada = $carbonHora->toTimeString();
    
                            $varw = Detalle_planificacion::create([
                                'planificacion_id' => $idPlanificacion,
                                'receta_id'=> $recetas[$key],
                                'hora_comida'=> $horaFormateada,
                                'estatus'=> 'activo',
                                'dia_id' =>$dias[$key],
                                'fechafin' =>'2024-02-06',
                                'fechainicio' =>'2024-02-06',
                            ]);

    
                        }
        
                        return response()->json(['success' => ['Planificación Guardada']]);
                    }
                   
                } else {
    

                    $fotoRecetaFile = $request->foto_planificacion;
                    $fileName = $fotoRecetaFile->getClientOriginalName();
                    $filePath = 'planificacion/' . Str::slug($fileName);
                    Storage::disk('public')->put($filePath, file_get_contents($fotoRecetaFile));
    
                    $arera = Planificacion::where('id', $request->input('idPlanificacion'))
                        ->update([
                            'estatus'=>'activo',
                            'foto_planificacion' => $filePath,
                        ]);
    
                    // Obtiene los datos del formulario
                    $recetas = $request->input('recetas');
                    $hora_inicio_inputs = $request->input('hora_inicio');
                    $dias = $request->input('dias');
                    $idPlanificacion = $request->input('idPlanificacion');
                    $fechafin = $request->input('fechafin');
                    $fechainicio = $request->input('fechainicio');


                    if (filled($fechainicio) && filled($fechafin)) {
                        Planificacion::where('id',$idPlanificacion)
                        ->update([
                            'estatus'=>'activo',
                            'fechafin'=>$fechafin,
                            'fechainicio'=>$fechainicio,
                        ]);
                    }
    
                    // Procesa y guarda los datos en la base de datos
                    foreach ($hora_inicio_inputs as $key => $hora_inicio) {
                      
                        $carbonHora = Carbon::parse($hora_inicio);
                        $horaFormateada = $carbonHora->toTimeString();


                        $varw = Detalle_planificacion::create([
                            'planificacion_id' => $idPlanificacion,
                            'receta_id'=> $recetas[$key],
                            'hora_comida'=> $horaFormateada,
                            'estatus'=> 'activo',
                            'dia_id' =>$dias[$key],
                            'fechafin' =>'2024-02-06',
                            'fechainicio' =>'2024-02-06',
                        ]);
                    }
                    return response()->json(['success' => ['Planificacion actualizada']]);
                }
            } catch (\Illuminate\Validation\ValidationException $e) {
                return response()->json(['error' => ['Los datos no cumplen los requisitos']]);
            }
        }



        if ($user->hasRole('cliente')) {

            try {

                $hora_inicio_inputs = $request->input('hora_inicio');
                $dias = $request->input('dias');
                $idPlanificacion = $request->input('planificacion_id');
                $recetas = $request->input('recetas');

                // Procesa y guarda los datos en la base de datos
                foreach ($hora_inicio_inputs as $key => $hora_inicio) {

                    $carbonHora = Carbon::parse($hora_inicio);
                    $horaFormateada = $carbonHora->toTimeString();


                    $subidaDato = [
                        'user_id' => $user->id,
                        'planificacion_id' => $idPlanificacion,
                        'receta_id' => $recetas[$key],
                        'hora_comida' => $horaFormateada,
                        'dias' => $dias[$key],
                    ];

                    $varw = Detalle_planificacion_usuario::where('planificacion_id',$subidaDato['planificacion_id'])
                    ->where('user_id',$subidaDato['user_id'])
                    ->where('receta_id',$subidaDato['receta_id'])
                    ->first();


                    if ($varw) {
                        Detalle_planificacion_usuario::where('planificacion_id',$subidaDato['planificacion_id'])
                        ->where('user_id',$subidaDato['user_id'])
                        ->where('receta_id',$subidaDato['receta_id'])
                        ->update([
                            'hora_comida' => $subidaDato['hora_comida'],
                            'dia_id' => $subidaDato['dias'],
                        ]);

                    } else {

                        $varw = Detalle_planificacion_usuario::create([
                            'user_id' => $subidaDato['user_id'],
                            'planificacion_id' => $subidaDato['planificacion_id'],
                            'receta_id' => $subidaDato['receta_id'],
                            'hora_comida' => $subidaDato['hora_comida'],
                            'dia_id' => $subidaDato['dias'],
                            'estatus' => 'activo'
                        ]);
                    }
                }
                return response()->json(['success' => ['Planificación Guardada']]);
            } catch (\Illuminate\Validation\ValidationException $e) {


                return response()->json(['error' => ['Los datos no cumplen los requisitos']]);
            }
        }
        

    }

    public function eliminarDatos($id)
    {
        $ingrediente = Detalle_planificacion::findOrFail($id);
        $ingrediente->delete();
        return response()->json(['mensaje' => 'Receta eliminado con éxito de planificacion']);
    }
    // public function save_planificacion(Request $request)
    // {
    //     // return response()->json(['error' => $request->all()]);

    //     if ($request->foto_receta != '{}') {
    //         $validator = Validator::make($request->all(), [
    //             'foto_receta' => 'required|file|mimes:png,jpg,jpeg',
    //             'nombre_receta' => 'required|string|max:255',
    //             'tiempo_preparacion' => 'required|string|max:255',
    //             'porciones' => 'required|string|max:255',
    //             'calorias' => 'required|string|max:255',
    //             'categoria' => 'required|string|max:255',
    //             'preparacion' => 'required|string',
    //         ]);

    //         if ($validator->fails()) {
    //             return response()->json(['error' => ['Los datos no cumples los requisitos(Foto Cambia)']]);
    //         }

    //         $fotoRecetaFile = $request->foto_receta;
    //         $fileName = $fotoRecetaFile->getClientOriginalName();
    //         $filePath = 'recetas/' . Str::slug($request->nombre_receta) . '/' . Str::slug($fileName);
    //         Storage::disk('public')->put($filePath, file_get_contents($fotoRecetaFile));
    //         $datosRecetas = [
    //             'foto_receta' => $filePath,
    //             'preparacion' => $request->preparacion,
    //             'tiempo_preparacion' => $request->tiempo_preparacion,
    //             'porciones' => $request->porciones,
    //             'calorias' => $request->calorias,
    //             'estatus' => 'activo',
    //             'categoria_id' => $request->categoria,
    //             'slug' => $request->slug,
    //         ];
    //         $idReceta = Receta::where('slug', $datosRecetas['slug'])
    //             ->first();
    //         if ($idReceta) {
    //             $idReceta->update($datosRecetas);
    //             // alert()->info('Receta existente', 'Receta actualizada');
    //             return response()->json(['success' => ['Receta actualizada']]);
    //         }
    //     } else {
    //         // return response()->json(['success' => $request->all()]);

    //         $validator = Validator::make($request->all(), [
    //             'tiempo_preparacion' => 'required|string|max:255',
    //             'porciones' => 'required|string|max:255',
    //             'calorias' => 'required|string|max:255',
    //             'categoria' => 'required|string|max:255',
    //             'preparacion' => 'required|string',
    //         ]);

    //         if ($validator->fails()) {
    //             return response()->json(['error' => ['Los datos no cumples los requisitos(Foto no cambia)']]);
    //         }

    //         $datosRecetas = [
    //             'preparacion' => $request->preparacion,
    //             'tiempo_preparacion' => $request->tiempo_preparacion,
    //             'porciones' => $request->porciones,
    //             'calorias' => $request->calorias,
    //             'estatus' => 'activo',
    //             'categoria_id' => $request->categoria,
    //             'slug' => $request->slug,
    //         ];
    //         $idReceta = Receta::where('slug', $datosRecetas['slug'])
    //             ->first();
    //         if ($idReceta) {
    //             $idReceta->update($datosRecetas);
    //             // alert()->info('Receta existente', 'Receta actualizada');
    //             return response()->json(['success' => ['Receta actualizada']]);
    //         }
    //     }
    // }

    public function guarda(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'foto_planificacion' => 'required|file|mimes:png,jpg,jpeg',
            'nombre_planificacion' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            alert()->error('error', 'Error al registrar');
        }

        $fotoRecetaFile = $request->foto_planificacion;
        $fileName = $fotoRecetaFile->getClientOriginalName();
        $filePath = 'planificacion/' . Str::slug($request->nombre_planificacion) . '/' . Str::slug($fileName);
        Storage::disk('public')->put($filePath, file_get_contents($fotoRecetaFile));


        $datosPlanificacion = [
            'foto_planificacion' => $filePath,
            'estatus' => 'por-activar',
            'nombre_planificacion' => $request->nombre_planificacion,
            'slug' => Str::slug($request->nombre_planificacion),
        ];
        $idReceta = Planificacion::where('slug', $datosPlanificacion['slug'])
            ->first();

        if ($idReceta) {
            // $idReceta->update($datosPlanificacion);

            alert()->info('Planificacion Existente', 'Ya existe la planificacion');
            return redirect()->route('admin.planificacion');
        } else {
            Planificacion::create([
                'foto_planificacion' => $datosPlanificacion['foto_planificacion'],
                'estatus' => $datosPlanificacion['estatus'],
                'nombre' => $datosPlanificacion['nombre_planificacion'],
                'slug' => $datosPlanificacion['slug'],
            ]);
            alert()->success('Planificación', 'Planificación Guardada');
            return redirect()->route('admin.planificacion');
        }
    }
}
