<?php

namespace App\Http\Controllers;

use App\Models\Detalle_planificacion;
use Illuminate\Http\Request;
use App\Models\Historial_Receta;
use Illuminate\Support\Facades\Auth;
use App\Models\Planificacion;
use App\Models\User;
use App\Models\Detalle_planificacion_usuario;
use Spatie\Permission\Traits\HasRoles;

class FavoritosController extends Controller
{
    public function ver_favorito()
    {

        return view('cliente.ver-favorito');
    }

    public function historial_cliente()
    {
        $user = Auth::user();

        $historial=Historial_Receta::where('user_id',$user->id)
            ->get();

        return view('cliente.historial-cliente', compact('historial'));

    }

    public function ver_planificacion($row)
    {
        $user = User::role('admin')->first();
        // dd($user->name);
        $id = $row;
        $idPlanificacion = Planificacion::find($row);
        if (!$idPlanificacion) {
            alert()->success('Error', 'Error al ejecutar consulta de planificacion');
        }
        $data = Detalle_planificacion::where('planificacion_id', $idPlanificacion->id)
        ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
        ->leftjoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
        ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
        ->select(
            'recetas.nombre_receta as nombre_receta',
            'detalle_planificacions.hora_comida as hora_comida',
            'detalle_planificacions.id as id',
            'dias.nombre as nombre',
            'categorias.nombre_categoria as nombre_categoria'
        )
        ->get();
        $idReceta = $idPlanificacion;

        return view('cliente.planificaciones',  compact('idReceta', 'data','user','id'));

    }
}
