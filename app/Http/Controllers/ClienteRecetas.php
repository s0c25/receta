<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Receta;
use App\Models\Planificacion;
use App\Models\Planifica_fech;
use App\Models\User;
use App\Models\Comunicacion;
use App\Models\pibot_planificaciones;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class ClienteRecetas extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $idReceta =  Receta::where('estatus', 'activo')->get();
        // dd('dsada');
        return view('dashboard-cliente', compact('idReceta'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function vence()
    {
        $user = Auth::user();
        // $datos = pibot_planificaciones::where('user_id', $user->id)
        //     ->get();
        // // dd($datos);
        // if ($datos->isEmpty()) {
        //     return view('dashboard-cliente');
        // }

        $planificaciones = Planifica_fech::where('planifica_feches.user_id', $user->id)
            ->where(function ($query) {
                $query->whereDate('planifica_feches.fechafin', Carbon::now()->format('Y-m-d'))
                    ->orWhereDate('planifica_feches.fechafin', Carbon::now()->addDay()->format('Y-m-d'));
            })
            ->leftJoin('planificacions', 'planificacions.id', '=', 'planifica_feches.planificacion_id')
            ->select('planificacions.nombre', 'planificacions.id', 'planifica_feches.fechafin')
            ->get();

        if ($planificaciones->isEmpty()) {
            return view('dashboard-cliente');
        }

        // foreach ($planificaciones as $planificacion) {
        //     // Puedes acceder a las propiedades de cada elemento
        //     $nombre = $planificacion->nombre;
        //     $id = $planificacion->id;
        //     $fechafin = $planificacion->fechafin;

        //     // Hacer lo que necesites con los datos
        //     echo "Nombre: $nombre, ID: $id, Fecha Fin: $fechafin";
        // }

        $resultados = $planificaciones;
        // // dd($resultados);
        if ($planificaciones->count() == 0) {
            return view('dashboard-cliente');
        } else {
            return view('vence', compact('resultados'));
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function send(Request $request)
    {

        if ($request->filled('message')) {
            $user = Auth::user();
            Comunicacion::create([
                'user_id' => $user->id,
                'mensaje' => $request->message,
                'mensaje_admin' => 'null',

            ]);
            alert()->success('Enviado', 'Mensaje enviado');
            return back();
        } else {
            alert()->info('Mensaje vacio', 'Debes llenar el campo mensaje');
            return back();
        }
    }

    /**
     * Display the specified resource.
     */
    public function mensajes()
    {
        $user = Auth::user();

        $comu =  Comunicacion::where('estatus', 'activo')
            ->where('user_id', $user->id)->get();
        return view('mensajes', compact('comu'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function response(Request $request)
    {

        if ($request->filled('repost')) {
            Comunicacion::where('id', $request->id)
                ->update([
                    'estatus' => 'respondido',
                    'mensaje_admin' => $request->repost
                ]);

            alert()->info('Enviado', 'Mensaje enviado');
            return redirect()->route('vermensajes');
        } else {
            alert()->info('Mensaje vacio', 'Debes llenar el campo mensaje');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
