<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Planificacion;
use App\Models\Planifica_fech;
use App\Models\Detalle_planificacion;
use App\Models\Detalle_planificacion_usuario;
use App\Models\Receta;
use App\Models\User;
use App\Models\Receta_Ingrediente;
use App\Models\pibot_planificaciones;
use App\Models\Dia;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade\Pdf as PDF;

class Planificacion_clientes extends Controller
{

    public function eliminarDatos($id)
    {

        $ingrediente = Detalle_planificacion_usuario::findOrFail($id);
        $ingrediente->delete();
        return response()->json(['mensaje' => 'Receta eliminado con éxito de planificacion']);

    }

    public function fot()
    {
        $user = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->first();
        return $user;
    }

    public function imprimir_recetas($row)
    {
        $user = Auth::user();

        $idReceta = Receta::find($row);

        $data = Receta_Ingrediente::where('id_receta', $idReceta->id)
            ->leftjoin('ingredientes', 'ingredientes.id', '=', 'receta__ingredientes.id_ingrediente')
            ->leftjoin('medidas', 'medidas.id', '=', 'receta__ingredientes.id_unidad')
            ->select(
                'ingredientes.nombre_ingrediente as nombre_ingrediente',
                'medidas.nombre_unidad as nombre_unidad',
                'receta__ingredientes.cantidad as cantidad',
            )
            ->get();

        $data = [
            'idReceta' => $idReceta,
            'datas' => $data,

        ];

        // Genera el PDF a partir de la vista
        $pdf = PDF::loadView('pdf.index-receta', $data);
        return $pdf->download($idReceta->nombre_receta . '.pdf');
    }

    public function verplanificacion($row)
    {

        $idPlanificacion = Planificacion::find($row);
        $user = $this->fot();

        if ($idPlanificacion->estatus == "por-activar") {

            alert()->error('Error', 'Uys, esta planificacion no tiene receta.');
            return redirect()->route('dashboard-cliente');
        }

        $data = Detalle_planificacion::where('planificacion_id', $idPlanificacion->id)
            ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
            ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
            ->select(
                'recetas.nombre_receta as nombre_receta',
                'dias.nombre as nombre',
                'recetas.calorias as calorias',
                'detalle_planificacions.hora_comida as hora_comida',
                'detalle_planificacions.id as id',
                'detalle_planificacions.dia_id as detalle_planificacions_dia_id',
                'detalle_planificacions.receta_id as detalle_planificacions_receta_id',
                'detalle_planificacions.planificacion_id as detalle_planificacions_planificacion_id',
                'detalle_planificacions.hora_comida as detalle_planificacions_hora_comida',
                'detalle_planificacions.fechafin as detalle_planificacions_fechafin',
                'detalle_planificacions.fechainicio as detalle_planificacions_fechainicio',

            )
            ->get();

        $idReceta = $idPlanificacion;
        return view('cliente.ver-planificacion', compact('idReceta', 'data', 'user'));
    }

    public function index()
    {
        $idPlanificacion = Planificacion::first();
        $user = Auth::user();

        if ($idPlanificacion == null) {
            alert()->error('Error', 'No tenemos planificaciones');
            return redirect()->route('dashboard-cliente');
        }
        $data = Detalle_planificacion::where('planificacion_id', $idPlanificacion->id)
            ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
            ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
            ->select(
                'recetas.nombre_receta as nombre_receta',
                'dias.nombre as nombre',
                'detalle_planificacions.hora_comida as hora_comida',
                'detalle_planificacions.id as id',
                'detalle_planificacions.dia_id as detalle_planificacions_dia_id',
                'detalle_planificacions.receta_id as detalle_planificacions_receta_id',
                'detalle_planificacions.planificacion_id as detalle_planificacions_planificacion_id',
                'detalle_planificacions.hora_comida as detalle_planificacions_hora_comida',

            )
            ->get();
        $idReceta = $idPlanificacion;
        return view('cliente.planificaciones', compact('idReceta', 'data', 'user'));
    }

    public function editar($row)
    {
        $user = Auth::user();
        $recetasall = Receta::get()->where('estatus', 'activo');

        // dd($user);
        $rows = Detalle_planificacion_usuario::where('planificacion_id', $row)
            ->where('user_id', $user->id)
            ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacion_usuarios.receta_id')
            ->leftjoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
            ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacion_usuarios.dia_id')
            ->select(
                'recetas.nombre_receta as nombre_receta',
                'recetas.id as id_receta',
                'dias.nombre as nombre_dia',
                'recetas.calorias as calorias',
                'categorias.nombre_categoria as nombre_categoria',
                'detalle_planificacion_usuarios.id as id',
                'detalle_planificacion_usuarios.hora_comida as hora_comida',
                'detalle_planificacion_usuarios.id as detalle_planificacion_usuariosid',
                'detalle_planificacion_usuarios.fechafin as detalle_planificacion_usuariosfechafin',
                'detalle_planificacion_usuarios.fechainicio as detalle_planificacion_usuariosfechainicio',

            )
            ->get();
        $dias = Dia::get();

        $idPlanificacion = Planificacion::find($row);

        $ise = Planifica_fech::where('planificacion_id', $row)
            ->where('user_id', $user->id)
            ->first();

        if($ise){
            // dd($ise);
            $fechasUnicas= 'null';
            $fechasInicio= 'null';
            $fechasFin= 'null';
            return view('cliente.list-planificacion-cliente', compact('recetasall','fechasUnicas','ise', 'rows', 'dias', 'idPlanificacion', 'fechasInicio', 'fechasFin'));

        }else{

            $fechas = $rows->pluck('detalle_planificacion_usuariosfechainicio', 'detalle_planificacion_usuariosfechafin');
            $fechasUnicas = $fechas->unique();
            $fechasInicio = $fechasUnicas->first();
            $fechasFin = $fechasUnicas->last();
    
    
            return view('cliente.list-planificacion-cliente', compact('recetasall','fechasUnicas', 'rows', 'dias', 'idPlanificacion', 'fechasInicio', 'fechasFin'));
        }
        
        
    }

    public function guarda_planificacion(Request $request)
    {

        $user = Auth::user();
        // dd($request->all());
        $dias = $request->dias;
        $receta_id = $request->recetas;
        $planificacion_id = $request->idPlanificacion;
        $hora_comida = $request->hora_inicio;
        $fechafin = $request->input('fechafin');
        $fechainicio = $request->input('fechainicio');
        // dd(filled($fechainicio));
        if (filled($fechainicio) && filled($fechafin)) {
            $ise = Planifica_fech::where('planificacion_id', $planificacion_id)
            ->where('user_id', $user->id)
            ->firstOrNew();
        
            // Verificar si el registro ya existía
            if ($ise->exists) {
                // Si el registro ya existía, actualiza los campos
                $ise->update([
                    'fechafin' => $fechafin,
                    'fechainicio' => $fechainicio,
                ]);
            } else {
                // Si el registro no existía, establece los campos y guarda el nuevo registro
                $ise->planificacion_id = $planificacion_id;
                $ise->user_id = $user->id;
                $ise->fechafin = $fechafin;
                $ise->fechainicio = $fechainicio;
                $ise->save();
            }
            
        }
        if (in_array(null, $dias, true) || in_array(null, $receta_id, true) || in_array(null, $hora_comida, true)) {

        } else {

            foreach ($dias as $key => $dia) {

                $data = Detalle_planificacion_usuario::where('user_id', $user->id)
                    ->where('receta_id', $receta_id[$key])
                    ->where('planificacion_id', $planificacion_id)
                    ->where('estatus', 'activo')
                    ->first();

                if ($data) {
                    Detalle_planificacion_usuario::where('user_id', $user->id)
                        ->where('receta_id', $receta_id[$key])
                        ->where('planificacion_id', $planificacion_id)
                        ->update([
                            'hora_comida' => $hora_comida[$key],
                            'dia_id' => $dia,
                            'receta_id'=> $receta_id[$key],
                        ]);
                } else {
                // dd('$data');
                    Detalle_planificacion_usuario::create([
                                'hora_comida' => $hora_comida[$key],
                                'dia_id' => $dia,
                                'receta_id'=> $receta_id[$key],
                                'planificacion_id'=>$planificacion_id,
                                'user_id'=>$user->id,

                            ]);
                }
            }
        }

        alert()->info('Agregado con exito', 'Planificacion agregada');
        return back();
    }

    public function imprimir($row)
    {
        $user = Auth::user();

        $wa = Planifica_fech::where('planificacion_id', $row)
             ->where('user_id', $user->id)
             ->first();
             $rows = Detalle_planificacion_usuario::where('planificacion_id', $row)
             ->where('user_id', $user->id)
             ->leftJoin('recetas', 'recetas.id', '=', 'detalle_planificacion_usuarios.receta_id')
             ->leftJoin('categorias', 'categorias.id', '=', 'recetas.categoria_id')
             ->leftJoin('dias', 'dias.id', '=', 'detalle_planificacion_usuarios.dia_id')
             ->select(
                 'recetas.nombre_receta as nombre_receta',
                 'recetas.id as id_receta',
                 'recetas.calorias as calorias',
                 'dias.nombre as nombre_dia',
                 'categorias.nombre_categoria as nombre_categoria',
                 'detalle_planificacion_usuarios.hora_comida as hora_comida',
                 'detalle_planificacion_usuarios.id as detalle_planificacion_usuariosid'
             )
             ->orderBy('detalle_planificacion_usuarios.hora_comida')
             ->get();
         
        $dialunes = [];
        $martes = [];
        $miercoles = [];
        $jueves = [];
        $viernes = [];
        $sabado = [];
        $domingo = [];

        // Iterar sobre los resultados y agruparlos por día
        foreach ($rows as $row) {
            $nombreDia = $row->nombre_dia;

            // Determinar en qué array guardar los datos según el día
            if ($nombreDia == 'Lunes') {
                $arrayDia = &$dialunes;
            } elseif ($nombreDia == 'Martes') {
                $arrayDia = &$martes;
            } elseif ($nombreDia == 'Miercoles') {
                $arrayDia = &$miercoles;
            } elseif ($nombreDia == 'Jueves') {
                $arrayDia = &$jueves;
            } elseif ($nombreDia == 'Viernes') {
                $arrayDia = &$viernes;
            } elseif ($nombreDia == 'Sabado') {
                $arrayDia = &$sabado;
            } elseif ($nombreDia == 'Domingo') {
                $arrayDia = &$domingo;
            }

            // Agregar los datos de esta receta al array correspondiente al día
            $arrayDia[] = [
                'nombre_receta' => $row->nombre_receta,
                'id_receta' => $row->id_receta,
                'calorias' => $row->calorias,
                'nombre_categoria' => $row->nombre_categoria,
                'hora_comida' => $row->hora_comida,
                'detalle_planificacion_usuariosid' => $row->detalle_planificacion_usuariosid
            ];
        }



      

        // dd($rows);
        $data = [
            'data' => $rows,
            'wa' => $wa,
            'dialunes' => $dialunes,
            'martes' => $martes,
            'miercoles' => $miercoles,
            'jueves' => $jueves,
            'viernes' => $viernes,
            'sabado' => $sabado,
            'domingo' => $domingo,
        ];

        // Genera el PDF a partir de la vista
        $pdf = PDF::loadView('pdf.index', $data);
        $pdf->setPaper('letter', 'landscape');
        return $pdf->download('planifica' . '.pdf');
    }

    public function save_cliente(Request $request)
    {
        $user = Auth::user();

        $dias = $request->dia;
        $receta_id = $request->receta_id;
        $planificacion_id = $request->planificacion_id;
        $hora_comida = $request->hora_comida;
        $fechafin = $request->fechafin;
        $fechainicio = $request->fechainicio;

        $data = pibot_planificaciones::where('user_id', $user->id)
            ->where('planificacion_id', $planificacion_id)
            ->first();
        if ($data) {

        } else {

            $data = pibot_planificaciones::create([
                'user_id' => $user->id,
                'planificacion_id' => $planificacion_id,
            ]);

            Planifica_fech::create([
                'user_id' => $user->id,
                'planificacion_id' => $planificacion_id,
                'fechafin' => $fechafin,
                'fechainicio' => $fechainicio,

            ]);
        }


        foreach ($dias as $key => $dia) {

            $data = Detalle_planificacion_usuario::where('user_id', $user->id)
                ->where('dia_id', $dia)
                ->where('receta_id', $receta_id[$key])
                ->where('planificacion_id', $planificacion_id)
                ->first();

            if ($data) {
                Detalle_planificacion_usuario::where('user_id', $user->id)
                    ->where('dia_id', $dia)
                    ->where('receta_id', $receta_id[$key])
                    ->where('planificacion_id', $planificacion_id)
                    ->update([
                        'hora_comida' => $hora_comida[$key],
                        'fechafin' => $fechafin,
                        'fechainicio' => $fechainicio,
                    ]);
            } else {
                Detalle_planificacion_usuario::create([
                    'user_id' => $user->id,
                    'dia_id' => $dia,
                    'receta_id' => $receta_id[$key],
                    'planificacion_id' => $planificacion_id,
                    'hora_comida' => $hora_comida[$key],
                    'fechafin' => $fechafin,
                    'fechainicio' => $fechainicio,
                ]);
            }
        }

        alert()->info('Agregado con exito', 'Planificacion agregada');
        return back();
    }
    public function mostrarSiguientePagina($currentId)
    {
        $user = $this->fot();

        $nextId = Planificacion::where('estatus','activa')->where('id', '>', $currentId)->min('id');
        if ($nextId === null) {
            $nextId = Planificacion::min('id');
        }
        if ($nextId !== null) {
            $idReceta = Planificacion::find($nextId);
            if (!$idReceta) {
                alert()->success('Error', 'Error al ejecutar consulta de planificacion');
            }
            $data = Detalle_planificacion::where('planificacion_id', $idReceta->id)
                ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
                ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
                ->select(
                    'recetas.calorias as calorias',
                    'recetas.nombre_receta as nombre_receta',
                    'dias.nombre as nombre',
                    'detalle_planificacions.hora_comida as hora_comida',
                    'detalle_planificacions.id as id',
                )
                ->get();
            $recorrido_mas = $idReceta->id;
            return view('cliente.ver-planificacion', compact('user', 'idReceta', 'data', 'recorrido_mas'));
        }
    }

    public function mostrarPaginaAnterior($currentId)
    {
        $user = $this->fot();

        $prevId = Planificacion::where('estatus','activa')->where('id', '<', $currentId)->max('id');
        if ($prevId === null) {
            $prevId = Planificacion::max('id');
        }
        $idReceta = Planificacion::find($prevId);
        if (!$idReceta) {
            alert()->success('Error', 'Error al ejecutar consulta de receta');
            return redirect()->back();
        }
        $data =  Detalle_planificacion::where('planificacion_id', $idReceta->id)
            ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacions.receta_id')
            ->leftjoin('dias', 'dias.id', '=', 'detalle_planificacions.dia_id')
            ->select(
                'recetas.calorias as calorias',
                'recetas.nombre_receta as nombre_receta',
                'dias.nombre as nombre',
                'detalle_planificacions.hora_comida as hora_comida',
                'detalle_planificacions.id as id',
            )
            ->get();
        $recorrido_menos = $idReceta->id;
        return view('cliente.ver-planificacion', compact('user', 'idReceta', 'data', 'recorrido_menos'));
    }
}
