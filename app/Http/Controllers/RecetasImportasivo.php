<?php

namespace App\Http\Controllers;

use App\Imports\RecetasImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Models\Receta;
use App\Models\Medida;
use App\Models\Ingrediente;
use App\Models\Categoria;
use App\Models\Receta_Ingrediente;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Storage;

class RecetasImportasivo extends Controller
{

    function formatearTexto($texto)
    {
        // Convertir a minúsculas
        $texto = strtolower($texto);

        // Eliminar espacios
        $texto = str_replace(' ', '', $texto);

        // Eliminar acentos
        $texto = iconv('UTF-8', 'ASCII//TRANSLIT', $texto);

        // Eliminar caracteres no deseados (puedes ajustar la expresión regular según tus necesidades)
        $texto = preg_replace('/[^a-zA-Z0-9]/', '', $texto);

        return $texto;
    }


    public function save(Request $request)
    {

        $user_id = Auth::id();
        $user_id = User::find($user_id);
        Cache::forget('importacion_en_proceso');

        if (Cache::get('importacion_en_proceso')) {
            return response()->json(['mensaje' => 'Ya hay una importación en proceso Usuario ' . $user_id->name . ' ' . $user_id->dni]);
        }
        $request->validate([
            'foto_receta' => 'required|file|mimes:csv',
        ]);

        $archivo = $request->file('foto_receta');


        if ($archivo && $archivo->isValid()) {
        } else {
            Cache::forget('importacion_en_proceso');

            return response()->json(['error_data' => [$archivo]]);
        }


        try {
            // Cache::put('importacion_en_proceso', true, 60); // Duración de la caché en minutos

            $import = new RecetasImport;

            Excel::import($import, $archivo);
            $collection = $import->data;

            foreach ($collection as $row) {
                $nombre_receta = $row['nombre_receta'];
                $foto_receta = $row['foto_receta'];
                $preparacion = $row['preparacion'];
                $tiempo_preparacion = $row['tiempo_preparacion'];
                $porciones = $row['porciones'];
                $calorias = $row['calorias'];
                $estatus = $row['estatus'];
                $cantidad = $row['cantidad'];
                $nombre_ingrediente = $row['nombre_ingrediente'];
                $nombre_unidad = $row['nombre_unidad'];
                $categoria_receta = $row['categoria_receta'];

                DB::beginTransaction();

                $url = $foto_receta;
              
                // Crea una instancia del cliente Guzzle
                $client = new GuzzleClient();

                // Realiza una solicitud GET a la URL
                $response = $client->get($url);

                // Obtiene el contenido de la respuesta (datos binarios de la imagen)
                $imageData = $response->getBody()->getContents();
                $fileName = basename($url);

                // Guarda la imagen en la carpeta "public" dentro del sistema de archivos de Laravel
                $filePath = Storage::disk('public')->put(Str::slug($fileName), $imageData);

                $nombrecatfot = $this->formatearTexto($categoria_receta);
                $datosCategoria = [
                    'nombre_categoria' => $nombrecatfot,
                    'slug' => Str::of($nombrecatfot)->slug('-'),
                ];
                $idCategoria = Categoria::where('slug', $datosCategoria['slug'])
                    ->first();

                if ($idCategoria) {
                    // $idCategoria->update($datosCategoria);
                } else {
                    $idCategoria =  Categoria::create([
                        'nombre_categoria' => $nombrecatfot,
                        'slug' => Str::of($nombrecatfot)->slug('-'),
                    ]);
                }

                $datosRecetas = [
                    'nombre_receta' => $nombre_receta,
                    'foto_receta' => "storage/" . $fileName,
                    'preparacion' => $preparacion,
                    'tiempo_preparacion' => $tiempo_preparacion,
                    'porciones' => $porciones,
                    'calorias' => $calorias,
                    'estatus' => $estatus,
                    'categoria_id' => $idCategoria->id,
                    'slug' => Str::of($nombre_receta)->slug('-'),
                ];
                $idReceta = Receta::where('slug', $datosRecetas['slug'])
                    ->first();

                if ($idReceta) {
                    // alert()->success('Receta existente', 'La receta'.' '.$datosRecetas['nombre_receta'].' '.'existes con el nombre unico'.' '.$datosRecetas['slug']);
                    // return redirect()->route('admin.recetas');
                } else {
                    $idReceta =  Receta::create([
                        'nombre_receta' => $nombre_receta,
                        'preparacion' => $preparacion,
                        'tiempo_preparacion' => $tiempo_preparacion,
                        'porciones' => $porciones,
                        'calorias' => $calorias,
                        'categoria_id' => $idCategoria->id,
                        'slug' => Str::slug($nombre_receta),
                        'foto_receta' => $fileName,
                    ]);
                }



                $cantidad = explode(', ', $row['cantidad']);
                $nombresIngredientes = explode(', ', $row['nombre_ingrediente']);
                $unidades = explode(', ', $row['nombre_unidad']);
                
                // Arreglos para acumular datos
                $medidas = [];
                $ingredientes = [];
                $recetasIngredientes = [];
                
                foreach ($cantidad as $key => $cantidadItem) {
                    $nombreIngrediente = $nombresIngredientes[$key];
                    $unidad = $unidades[$key];
                
                    // Unidades
                    $datosMedida = [
                        'nombre_unidad' => $unidad,
                        'slug' => Str::of($unidad)->slug('-'),
                    ];
                    $medidas[$key] = Medida::updateOrCreate(
                        ['slug' => $datosMedida['slug']],
                        $datosMedida
                    );
                
                    // Ingredientes
                    $datosIngredientes = [
                        'nombre_ingrediente' => $nombreIngrediente,
                        'slug' => Str::of($nombreIngrediente)->slug('-'),
                    ];
                    $ingredientes[$key] = Ingrediente::updateOrCreate(
                        ['slug' => $datosIngredientes['slug']],
                        $datosIngredientes
                    );
                
                    // Receta_Ingredientes
                    $datosRecetasIngredientes = [
                        'cantidad' => $cantidadItem,
                        'id_unidad' => $medidas[$key]->id,
                        'id_ingrediente' => $ingredientes[$key]->id,
                        'id_receta' => $idReceta->id,
                        'estatus' => 'activo',
                    ];
                
                    $recetasIngredientes[$key] = $datosRecetasIngredientes;
                }
                
                // Insertar o actualizar todos los registros después de salir del bucle
                Receta_Ingrediente::upsert($recetasIngredientes, ['id_receta', 'id_ingrediente']);
                DB::commit();
                
            }


            Cache::forget('importacion_en_proceso');

            alert()->success('Exito', 'Importacion terminada');
            return redirect()->route('admin.recetas');

        } catch (\Exception $e) {
            DB::rollback();
            Cache::forget('importacion_en_proceso');
            alert()->success('Exito', 'Importacion terminada'.$e);

            return redirect()->route('admin.recetas');
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            DB::rollback();
            Cache::forget('importacion_en_proceso');
            $failures = $e->failures();
            alert()->success('Exito', 'Importacion terminada'.$failures);
            return redirect()->route('admin.recetas');
        }
    }
}
