<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Models\Receta;
use App\Models\Planificacion;
use App\Models\Planifica_fech;
use App\Models\pibot_planificaciones;
use Carbon\Carbon;


class checklogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $id = Auth::id();
        $user = User::find($id);
        if ($user->hasRole('admin')) {
            return $next($request);
        } else {
            $planificaciones = Planifica_fech::where('planifica_feches.user_id', $user->id)
                ->where(function ($query) {
                    $query->whereDate('planifica_feches.fechafin', Carbon::now()->format('Y-m-d'))
                        ->orWhereDate('planifica_feches.fechafin', Carbon::now()->addDay()->format('Y-m-d'));
                })
                ->leftJoin('planificacions', 'planificacions.id', '=', 'planifica_feches.planificacion_id')
                ->select('planificacions.nombre', 'planificacions.id', 'planifica_feches.fechafin')
                ->get();

            if ($planificaciones != '[]') {
                return redirect(RouteServiceProvider::VENCE);
            } else {
                // return $next($request);
                return redirect(RouteServiceProvider::CLIENTE);
            }
        }
    }
}
