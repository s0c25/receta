<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Models\Receta;
use App\Models\Planificacion;
use App\Models\Planifica_fech;
use App\Models\pibot_planificaciones;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class notifi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $id = Auth::id();
        // dd( $id);
        // Obtener el usuario por su ID
        $user = User::find($id);

        // Verificar si el usuario tiene un rol específico
        if ($user->hasRole('admin')) {

            return $next($request);
        } else {
            $planificaciones = Planifica_fech::where('planifica_feches.user_id', $user->id)
            ->whereDate('planifica_feches.fechainicio', Carbon::now()->format('Y-m-d'))
            ->orwhereBetween('planifica_feches.fechainicio', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDay()->endOfDay()->format('Y-m-d')])
            ->orwhereBetween('planifica_feches.fechainicio', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDays(2)->endOfDay()->format('Y-m-d')])
            ->orwhereBetween('planifica_feches.fechainicio', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDays(3)->endOfDay()->format('Y-m-d')])
            ->leftjoin('planificacions', 'planificacions.id', '=', 'planifica_feches.planificacion_id')
            // ->leftjoin('detalle_planificacion_usuarios', 'detalle_planificacion_usuarios.planificacion_id', '=', 'planifica_feches.planificacion_id')
            // ->leftjoin('recetas', 'recetas.id', '=', 'detalle_planificacion_usuarios.receta_id')
            ->select('planificacions.nombre','planificacions.id','planifica_feches.fechainicio')
            ->get();

            if($planificaciones->isEmpty()){

                $planificaciones = pibot_planificaciones::where('user_id', $user->id)
                ->leftjoin('planificacions', 'planificacions.id', '=', 'pibot_planificaciones.planificacion_id')
            ->whereDate('planificacions.fechainicio', Carbon::now()->format('Y-m-d'))
            ->orwhereBetween('planificacions.fechainicio', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDay()->endOfDay()->format('Y-m-d')])
            ->orwhereBetween('planificacions.fechainicio', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDays(2)->endOfDay()->format('Y-m-d')])
            ->orwhereBetween('planificacions.fechainicio', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDays(3)->endOfDay()->format('Y-m-d')])
                ->select('planificacions.nombre','planificacions.id')
                ->get();

            }


            // dd($planificaciones);
            if($planificaciones != '[]'){
                return redirect(RouteServiceProvider::VENCE);
            }else{
                return $next($request);

            }


    
        }
    }
}
