<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('detalle_planificacions', function (Blueprint $table) {
            $table->id();
            $table->string('estatus')->default('activo');
            $table->unsignedBigInteger('planificacion_id');
            $table->unsignedBigInteger('receta_id');
            $table->time('hora_comida'); // Hora en que se debe consumir la receta
            $table->timestamps();
            $table->unsignedBigInteger('dia_id');
            $table->foreign('dia_id')->references('id')->on('dias')->onDelete('cascade');
            $table->foreign('planificacion_id')->references('id')->on('planificacions')->onDelete('cascade');
            $table->foreign('receta_id')->references('id')->on('recetas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('detalle_planificacions');
    }
};
