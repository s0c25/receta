<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('planificacions', function (Blueprint $table) {
            $table->date('fechafin')->nullable(); // Hora en que se debe consumir la receta
            $table->date('fechainicio')->nullable(); // Hora en que se debe consumir la receta
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('planificacions', function (Blueprint $table) {
            //
        });
    }
};
