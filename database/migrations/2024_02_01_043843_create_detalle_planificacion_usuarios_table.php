<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('detalle_planificacion_usuarios', function (Blueprint $table) {
            $table->id();
            $table->string('estatus')->default('activo');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('planificacion_id');
            $table->time('hora_comida'); // Hora en que se debe consumir la receta
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('planificacion_id')->references('id')->on('planificacions')->onDelete('cascade');
            $table->unsignedBigInteger('receta_id');
            $table->foreign('receta_id')->references('id')->on('recetas')->onDelete('cascade');
            $table->unsignedBigInteger('dia_id');
            $table->foreign('dia_id')->references('id')->on('dias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('detalle_planificacion_usuarios');
    }
};
