<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('planifica_feches', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('planificacion_id');
            $table->foreign('planificacion_id')->references('id')->on('planificacions')->onDelete('cascade');

            $table->date('fechafin')->nullable(); // Hora en que se debe consumir la receta
            $table->date('fechainicio')->nullable(); // Hora en que se debe consumir la receta
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('planifica_feches');
    }
};
