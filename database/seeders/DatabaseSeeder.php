<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\Categoria;
use App\Models\Ingrediente;
use App\Models\Medida;
use App\Models\Dia;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Dia::create([
            'nombre' => 'Lunes',
        ]);
        Dia::create([
            'nombre' => 'Martes',
        ]);
        Dia::create([
            'nombre' => 'Miercoles',
        ]);
        Dia::create([
            'nombre' => 'Jueves',
        ]);
        Dia::create([
            'nombre' => 'Viernes',
        ]);
        Dia::create([
            'nombre' => 'Sabado',
        ]);
        Dia::create([
            'nombre' => 'Domingo',
        ]);

        // MwTYCSj3yY-gtdAVxEcmX


        $user = User::create([
            'name' => 'Nicolas Flamel',
            'email' => 'recetas@recetas.com',
            'password' => Hash::make('nicolasflamel500'),
        ]);

        Categoria::create([
            'nombre_categoria' => 'Desayuno',
            'slug' => Str::slug('desayuno'),
        ]);
        Categoria::create([
            'nombre_categoria' => 'Almuerzo',
            'slug' => Str::slug('almuerzo'),
        ]);
        Categoria::create([
            'nombre_categoria' => 'Cena',
            'slug' => Str::slug('cena'),
        ]);
        Ingrediente::create([
            'nombre_ingrediente' => 'Tomate',
            'slug' => Str::slug('Tomate'),
        ]);
        Ingrediente::create([
            'nombre_ingrediente' => 'Cebolla',
            'slug' => Str::slug('Cebolla'),
        ]);
        Ingrediente::create([
            'nombre_ingrediente' => 'Mani',
            'slug' => Str::slug('Mani'),
        ]);
        Medida::create([
            'nombre_unidad' => 'Tazas',
            'slug' => Str::slug('Tazas'),
        ]);
        Medida::create([
            'nombre_unidad' => 'Cucharadas',
            'slug' => Str::slug('Cucharadas'),
        ]);


        
        
        $role = Role::create(['name' => 'admin']);
        Role::create(['name' => 'cliente']);

        $user->assignRole($role);


        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
