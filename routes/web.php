<?php

use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\RecetasController;
use App\Http\Controllers\RecetasImportasivo;
use App\Http\Controllers\PlanificacionController;
use App\Http\Controllers\ClienteRecetas;
use App\Http\Controllers\FavoritosController;
use App\Http\Controllers\Planificacion_clientes;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/google-auth/redirect', function () {
    return Socialite::driver('google')->redirect();
})->name('google-redi');
 
Route::get('/google-auth/callback', function () {
    $user = Socialite::driver('google')->stateless()->user();

    $iduser=User::updateOrCreate([
        'google_id'=>$user->id,
    ],[
        'name' => $user->name,
        'email' => $user->email,
        'profile_photo_path' => $user->avatar,
    ]);
    $iduser->assignRole('cliente');

    Auth::login($iduser);

    return redirect('/dashboard');

    // $user->token
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified','checklogin'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get("/admin/recetas", [RecetasController::class, "index"])->name('admin.recetas');
    Route::post("/admin/recetas-save", [RecetasController::class, "save"])->name('admin.recetas-save');
    Route::post("/admin/masix-save", [RecetasImportasivo::class, "save"])->name('admin.masix-save');
    Route::get("/admin/recetas-view/{row}", [RecetasController::class, "view"])->name('admin.recetas-view');
    Route::get("/admin/recetas-edit/{row}", [RecetasController::class, "edit"])->name('admin.recetas-edit');
    Route::get('/admin/rdirect/{id}', [RecetasController::class, "mostrarSiguientePagina"])->name('siguiente.pagina');
    Route::get('/admin/edirect/{id}', [RecetasController::class, "mostrarPaginaAnterior"])->name('anterior.pagina');
    Route::post("/admin/save-receta", [RecetasController::class, "save_receta"])->name('admin.save-receta');
    Route::delete('/eliminar-datos/{id}', [RecetasController::class, 'eliminarDatos'])->name('eliminar.datos');
    Route::post('/guardar-datos', [RecetasController::class, 'guardarDatos'])->name('guardar.datos');

    //planificacion
    Route::get("/admin/planificacion", [PlanificacionController::class, "index"])->name('admin.planificacion');
    Route::post("/admin/planificacion-guarda", [PlanificacionController::class, "guarda"])->name('admin.planificacion-guarda');
    
    Route::get("/admin/planificacion-edit/{row}", [PlanificacionController::class, "edit"])->name('admin.edit-planificacion');
    Route::delete('/eliminar-planificacion/{id}', [PlanificacionController::class, 'eliminarDatos'])->name('eliminar.datos-planificacion');
    Route::post('/guardar-datos-planificacion-admin', [PlanificacionController::class, 'guardarDatos'])->name('save-planificacion');
    Route::post("/admin/save-planificacion", [PlanificacionController::class, "save_planificacion"])->name('admin.save-planificacion');
    Route::get("/admin/ver-planificacion/{row}", [PlanificacionController::class, "ver_planificacion"])->name('admin.ver-planificacion');
    Route::get('/admin/plani-net/{id}', [PlanificacionController::class, "mostrarSiguientePagina"])->name('siguiente.plani-net');
    Route::get('/admin/ante-net/{id}', [PlanificacionController::class, "mostrarPaginaAnterior"])->name('anterior.ante-net');
    Route::post('/admin/response-sugerencia', [ClienteRecetas::class, 'response'])->name('admin.response');


});

Route::get('vermensajes', function () {
    return view('vermensajes');
})->name('vermensajes');

Route::get('/glosario', function () {
    return view('glosario');
})->name('glosario');

Route::get('/test', function () {
    return view('pdf.indsex');
})->name('test');

Route::middleware([
    'auth'
])->group(function () {

    Route::delete('/cliente/eliminar-planificacion/{id}', [Planificacion_clientes::class, 'eliminarDatos'])->name('cliente.eliminar.datos-planificacion');


    Route::get('/dashboard-cliente', [ClienteRecetas::class, "index"])->name('dashboard-cliente');
    Route::get("/cliente/recetas-cliente", [RecetasController::class, "index"])->name('admin.recetas-cliente');
    Route::get('/cliente/rdirect/{id}', [RecetasController::class, "mostrarSiguientePagina"])->name('siguiente.pagina-cliente');
    Route::get('/cliente/edirect/{id}', [RecetasController::class, "mostrarPaginaAnterior"])->name('anterior.pagina-cliente');
    Route::get('/favorito/{id}', [RecetasController::class, "favorito"])->name('cliente.favorito');
    Route::get('/cliente/ver-favorito/', [FavoritosController::class, "ver_favorito"])->name('cliente.ver-favorito');
    Route::get("/cliente/recetas-cliente/{row}", [RecetasController::class, "view"])->name('cliente.ver-recetas-cliente');
    // Route::get("/cliente/delete/{row}", [RecetasController::class, "delete"])->name('cliente.delete-plani-fav');
    Route::get('/cliente/historial-cliente/', [FavoritosController::class, "historial_cliente"])->name('cliente.historial-cliente');
    Route::get("/cliente/planificacion/", [Planificacion_clientes::class, "index"])->name('cliente.planificacion');
    Route::get('/cliente/plani-net/{id}', [Planificacion_clientes::class, "mostrarSiguientePagina"])->name('siguiente.plani-net-cliente');
    Route::get('/cliente/ver-planificacion/{row}', [Planificacion_clientes::class, "verplanificacion"])->name('cliente.ver-planificacion');

    Route::get('/cliente/ante-net/{id}', [Planificacion_clientes::class, "mostrarPaginaAnterior"])->name('anterior.ante-net-cliente');
    Route::post('/cliente/planificacion/save', [Planificacion_clientes::class, 'save_cliente'])->name('cliente.planificacion_save');
    Route::get('/cliente/planificacion/listar', [Planificacion_clientes::class, 'listar'])->name('cliente.listar-planificaciones');
    Route::get('/cliente/planificacion/{row}', [Planificacion_clientes::class, 'imprimir'])->name('cliente.imprimir');
    Route::get('/cliente/planificacion-editar/{row}', [Planificacion_clientes::class, 'editar'])->name('cliente.editar-plani');
    Route::post("/cliente/planificacion-edit/", [Planificacion_clientes::class, "guarda_planificacion"])->name('guardar.planificacion');

    Route::get("/cliente/imprimir-receta/{row}", [Planificacion_clientes::class, "imprimir_recetas"])->name('cliente.recetas-imprimir');
    Route::get("/vence", [ClienteRecetas::class, "vence"])->name('vence');
    Route::post('/cliente/send-sugerencia', [ClienteRecetas::class, 'send'])->name('cliente.send');
    Route::get("/mensajes", [ClienteRecetas::class, "mensajes"])->name('mensajes');


    

});


